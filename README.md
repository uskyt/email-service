# email-service

By Ulrik Skyt, [ulrik.skyt@gmail.com](ulrik.skyt@gmail.com), 
[LinkedIn profile](https://www.linkedin.com/in/ulrikskyt/)

## Introduction
This project implements a backend service that accepts the necessary information for sending emails. 
It provides an abstraction between multiple different email service providers. 
If one of the service providers goes down, this service can quickly failover to a different provider without affecting its customers.

The service can use the following Email Providers:

 - [SendGrid](https://sendgrid.com/user/signup) - [Simple Send Documentation](https://sendgrid.com/docs/API_Reference/Web_API/message.html)
 - [Mailgun](http://www.mailgun.com/) - [Simple Send Documentation](http://documentation.mailgun.com/quickstart.html#sending-messages)
 - [SparkPost](https://www.sparkpost.com/) - [Developer Hub](https://developers.sparkpost.com/)
 - [Amazon SES](http://aws.amazon.com/ses/) - [Simple Send Documentation](http://docs.aws.amazon.com/ses/latest/APIReference/API_SendEmail.html)
 
The Email Providers require different levels of assurance that the sender addresses (From/Reply-to etc.) are validated 
as belonging to / associated with the sending account. In my case I have chosen to use the domain skytconsulting.dk, 
because it's a domain I already had (but was not in active use). This has required setting a number of different DNS
records. Because of this, there should be a good chance that messages don't automatically get flagged as spam.
For this reason my service is configured to only allow From-adresses on the domain skytconsulting.dk.

The same project also contains a simple client application. Actually it is almost just a static page. For that reason, 
I did not bother to separate it into its own independent project and/or deployable artifact. The focus of my efforts 
have been on the backend.


## Functionality overview

The main function is a simple REST service which accepts POST requests to the path:

 - https://email-service.skytconsulting.dk/rest/mail

with a JSON structure in the HTTP body, which defines the message, and optionally a query parameter to request the use
of a specific Email Provider (this is mostly for testing).

The application also exposes Swagger / OpenAPI documentation for the service. It is available at the paths:
 
 - https://email-service.skytconsulting.dk/swagger-ui.html (HTML)
 - https://email-service.skytconsulting.dk/v2/api-docs (OpenAPI JSON format)
 
Health checks and application information endpoints are availalbe on the following paths:
 
 - https://email-service.skytconsulting.dk/actuator/health
 - https://email-service.skytconsulting.dk/actuator/info

This is implemented using Spring Boot Actuator 2.x. The two mentioned functions are the only ones enabled, but a lot of 
additional functionality from Actuator can be enabled, should this be desired.
For more information on this, see [here](https://www.baeldung.com/spring-boot-actuators).
Enabling these two endpoints has no security implications, while enabling others would have (like 'shutdown' or others 
that could provoke excessive logging or reveil sensitive internal information).

The test client is available on:

 - https://email-service.skytconsulting.dk/

## Architecture

The application exposes a REST service which accepts requests to send emails asynchronously. I.e. the messages are 
validated and queued before the client receives a response, but the actual sending occurs decoupled in a separate thread
or maybe even in a different process. Under normal circumstances, messages will be sent immediately with no delay.

Definitions for the database tables can be found in the file [createDatabase.sql](src/scripts/db/createDatabase.sql).
In the database Messages have one of the statuses: 

 - REJECTED
 - QUEUED
 - SENT
 - ERROR_NOT_HANDLED
 - ERROR_HANDLED

Currently, there is no functionality that sets status to REJECTED or ERROR_HANDLED. Messages that fail validation when 
received are not stored in the database.

There is a record for each user, which primarily holds username, and a salt+hash for password validation. Furthermore,
the records can holde name and email information (which is not used by the application now), and they are used to 
collect a little set of information: AuthenticatedRequestsCount and AuthenticationFailedCount. Information like this
could be used to lock users after a number of unsuccessfull authentication attempts. But now they are just counters that
are never reset.   


### Security

Requests sent to the path /rest/* are covered by a ServletFilter called AuthenticationFilter. Its job is to ensure that
requests have provided a Basic Authorization with a valid username and password. The password storage and validation is 
implemented following the current best pratice, as described in these links:

 - https://crackstation.net/hashing-security.htm
 - https://www.baeldung.com/java-password-hashing

A "salt" is generated and saved for each account, using a Cryptographically Secure Pseudo-Random Number Generator.
Using salt prevents "dictionary attacks" where an attacker has a large pre-built database of hashes for known
strings.

Passwords are hashed together with their salt, using the "PBKDF2" algorithm, which is a deliberately "slow" hashing
algorithm. Using a slow algorithm makes it less feasible than otherwise to make brute force attacks.


### Normal flow

The architecture of the application is illustrated in the following figure, which illustrates a normal flow for sending 
a message.

![alt text](doc/NormalFlow.png)


1. The application receives a POST request. 
2. The AuthenticationFilter uses AuthenticationService to validate that the username and password are correct.
3. The AuthenticationService looks up the user record in the database by username by calling the UserDAO class.
4. After successful authentication, the EmailController class proceeds with handling the request. It is a Spring 
@RestController class. The accepted JSON format is defined by a set of POJO Value Objects, and 
serialization/deserialization is handled by the Jackson library (this is handled automatically in this case).
5. The EmailController's only responsibility is to expose a REST service, so it does nothing but delegate to a service 
class and method, QueueEmailService.submitSendMessageJob().
6. The message is validated to ensure that once accepted, it should be possible to pass on the message successfully on
to an email provider. The Validation is handled by a separate class, EmailValidator, which has this validation as its 
only responsibility.
7. If a specific email provider was requested, the given ProviderID is also validated.
8. If the message is accepted by the validator, it is saved in the database table Messages with status QUEUED. Database
operations are handled via a MessageDAO class, which is only responsible for database queries and mapping between Java 
objects and SQL. Saving the message in the database ensures that it is not lost, if the process dies before having 
delivered the message to an email provider.
9. A job is then scheduled via the JobScheduler class in a thread pool for execution as soon as possible. The idea is 
that during normal processing, the process that receives the message and queues it also handles it - but asynchronously
in a different thread. 
10. The JobScheduler performs the actual scheduling, using a ScheduledExecutorService, i.e. a standard Java thread pool.
11. When the job executes, all it does is to delegate on to SendEmailService.sendEmail().
12. The SendEmailService has a number of EmailProvider implementations, and calling them is done via a CircuitBreaker
from the Failsafe library. This particular CircuitBreaker implementation is very simple and easy to use. 
13. The strategy is to try one EmailProvider (and the associated CircuitBreaker) at the time until one accepts the 
email. 
14. If none of the EmailProviders are available or they all fail on the message - maybe because EmailValidator is
missing some important rule - then an error is logged and the message is parked with status ERROR_NOT_HANDLED (which 
could work like a "dead letter queue"). If the message is sent successfully, it is updated in the database with status 
SENT, the ProviderID of the EmailProvider used and the MessageID returned from that EmailProvider.

>**Note:** The application has no handling of messages which failed to be delivered to any EmailProvider, other than 
setting status to ERROR_NOT_HANDLED... Chances are that the error is in the message, rather than in all 4 EmailProviders 
One appropriate automated action could be to send an error email to the sender or to a person responsible for the 
applicaton using the service.

>**Note:** All messages are kept in the database. The applicaton has no functionality to clean up. Depending on
requirements, this can be a good or a bad thing. It would be simple to just delete messages instead of setting status
to SENT or ERROR_HANDLED. But maybe the messages are good to have for a period of time, to handle support cases, etc.

>**Note:** There has been done nothing to choose EmailProvider by preference, e.g. based on price, stability, response 
time, delivery time, spam-problems, preferred process for bounces and complaints (receivers marking email as spam) or 
other similar criteria. With any significant amount of email messaging, it would probably be better with a more 
deterministic strategy based on some of these criteria.

### Alternative flow

If the client sets a request parameter "EmailProvider" then the application enforces the use of that specific 
EmailProvider. This is done in the SendEmailService. If a specific provider has been specified, it constructs a
singleton-list with the specified EmailProvider. With this list (rather than lists of all EmailProviders), the normal 
flow can continue (step 12 in the figure above).

Possible values for the EmailProvider parameter are:

 - "amazon-ses"
 - "mailgun.com"
 - "sendgrid.com"
 - "sparkpost.com"


### Sending orfaned messages

If messages in the database does not have status SENT after some configurable period (which by default is 1 minute),
then a scheduled job will find them and send them. It is assumed that if this period has gone and the messages have not 
been sent, then it is because the process has died, and hence some other process must take over the responsibility of
sending the messages. The process of doing this is showed in the following figure, and explained in the numbered list 
below.

![alt text](doc/ScheduledJob.png)
 
 1. At application initialization time, a job is scheduled by the JobScheduler to run periodically (once every minute).
 2. The job looks allocates and then reads QUEUED (unsent) messages in the database which have not been modified for 
 a configurable period of time (default is 60 seconds). This is done in a way that protects against race conditions 
 between different processes doing this simultaneously. If many messages are present, 50 are read processed at the 
 time, and then the process loops until all have been handled.
 3. If some messages were allocated/read then jobs are scheduled to handle them as soon as possible.
 4. When the jobs run, they call SendEmailService (like in the normal flow).
 5. The SendEmailService has a number of EmailProvider implementations, and calling them is done via a CircuitBreaker
 from the Failsafe library.  
 6. The strategy is to try one EmailProvider (and the associated CircuitBreaker) at the time until one accepts the 
 email. 
 7. If none of the EmailProviders are available or they all fail on the message - maybe because EmailValidator is
 missing some important rule - then an error is logged and the message is parked with status ERROR_NOT_HANDLED (which 
 could work like a "dead letter queue"). If the message is sent successfully, it is updated in the database with status 
 SENT, the ProviderID of the EmailProvider used and the MessageID returned from that EmailProvider.
 
 
 > **Note:** I know that the above figures do not follow a formal diagram style, like UML, but I often find that 
informal drawings with "boxes and lines" convey information easier for both the author and the reader.

## Design choices 

Single responsibility principle: A class should , if possible, have a single, well-defined responsibility.

Don't state the obvious in javadoc - like documenting the 'subject' field in the 'Message' class with 'The subject of the message'. 

Use native Java libraries from the email provider, if available.
Pros: This should be easier to use and less error prone, at least regarding proper use of the proprietary interface, 
because of the type-safety of the Java-classes.
Cons: Maybe you need to control things like setting request timeout in multiple different ways (for each library).

The implemented strategy should deliver correct Messages "at least once" rather than "at most once".


## Scope choices and improvement opportunities
To limit the scope of the assignment somewhat, I have desided on the following limitations.

 - Only support very simple emails (no html format, multipart, attachments, custom headers, etc.)
 
These are some areas where the solution could be improved if more time were to be invested:

 - Error handling in the EmailProvider implementations don't really distinguish between message-related errors (e.g. 
   status 4xx) and service-related errors (like status 5xx). So if the applications own validator is not strict enough 
   it may make a single bad message count as an error in all the circuit breakers. To make matters worse this could 
   potentially be abused deliberately by an attacker to put a stop to all message deliveries! But first this attacker
   should have a valid username and password.
 - The Messages table could be extended with more information, e.g. username of the authenticated user and perhaps the
   email address used as from-address.
 - In the health check it might be good to distinguish between liveness and readyness, e.g. by setting a status in a 
   Spring @PostConstruct method. However this is only useful if the deployment platform supports this.
 - Secret handling could be better. Locally, I have run with a 'secret.properties' file in the /src/main/resources and 
   /src/test/resources folders and added them to .gitignore to ensure it was not committed. But they should not be 
   packaged into the jar and docker image, so when I run a build they are excluded in the build-section of the pom.xml.
   But after the build I have to comment out the exclude of the pom.xml to use it locally. That is not good.
 - Testing could be more thorough. Maybe the logic in some of the  'service' classes should be tested using mocks.


## Deployment

The project can be built with the command:

```bash
mvn clean package
```

Then a bas docker image and the application docker image can be built using these commands:

```bash
docker build --tag=alpine-java:base --rm=true .
docker build --file=Dockerfile.server --tag=email-service:latest --rm=true .
```

The the docker image can run locally:

```bash
docker run --name=email-service --publish=8080:8080 email-service:latest
```

The (if the proper login credentials have been configured), the image can be published to my Aamazon Elastic Container 
Registry:

````bash
$(aws ecr get-login --region eu-west-1 --no-include-email)
docker tag email-service 968251542482.dkr.ecr.eu-west-1.amazonaws.com/email-service
docker push 968251542482.dkr.ecr.eu-west-1.amazonaws.com/email-service
````

> **Note:** The above probably ought to be baked into the build script, so that it could all be executed with a simple 
``mvn clean install`` or ``mvn clean deploy``.  


The project is hosted on Amazon Elastic Container Service (ECS) which keep 3 container instances running.

The database is a MariaDB instance hosted on Amazon Managed Relational Database Service (RDS).

In front of ECS is an Amazon Elastic Load Balancer (ELB), which terminates SSL and routes traffic to one of the 
container instances. 

>I had some trouble with the health checks from the ECS container itself, which kept failing. So I have disabled the 
containers own health check. There is still use of health checks from the load balancer. But I don't think that will 
trigger that an unhealthy container is retired and a new one started up.


## Special review attention

Places I would like to point out that deserve special attention from reviewers:

 - I like the clear split of responsibilities between the classes in the 'service' package.
 - RESTServiceTest. I like the way tests are structured, so that each test case has very little code, only what is 
 needed to express what is special to that particular test case.  
 - SendGridEmailProviderMockTest. Demonstrates that an EmailProvider is configured to send to an alterntive address, 
 where a mock-server is running. This tests that http timeout settings are working. More tests of this kind could 
 probably be written to test other aspects. This type of test is only done for one EmailProvider, for a real system the
 other EmailProviders should have similar tests.
 
Regarding applied technologies, where I don't have much experience, I would like to mention the following.
 
 - Docker is new to me. I've read about it, and appreciate the advantages it gives, but this is the first project 
   where I defined and used a Docker image.
 - I have not worked with AWS services before. Here I have used Simple Email Service (SES), Elastic Container Service 
   (ECS), Elastic Container Registry (ECR), Elastic Load Balancer (ELB), Certificate Manager and of course Identity 
   and Access Management (IAM).
 - Swagger and OpenAPI are also new to me. This is the first project, where I have defined and exposed API using Swagger.
 - I'm not an expert in email communication, so I've kept functionality simple. Even in spite of that, I may have 
   missed something that could be considered essential.
 - I don't claim to be an experienced frontend developer, so the client is very simple.
 
But I do consider myself experienced in the core technologies used in the (backend part of) the project:
 
  - Java
  - Spring
  - SQL (MySQL/MariaDB)
  - REST

as well as being experienced in general software engineering, IT architecture and development with a DevOps mindset.
 
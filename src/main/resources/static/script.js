
var nextID = 1;

$(document).ready(function() {
    $("#contact").submit(onSubmit);
    $("#reset").click(resetForm);
    $("#addTo").click(onAddToButtonClick);
    $("#addCC").click(onAddCcButtonClick);
    $("#addBCC").click(onAddBccButtonClick);
});


function onAddToButtonClick(event) {
    event.preventDefault();
    onAddButtonClick($(this), "toname", "toemail");
}

function onAddCcButtonClick(event) {
    event.preventDefault();
    onAddButtonClick($(this), "ccname", "ccemail");
}

function onAddBccButtonClick(event) {
    event.preventDefault();
    onAddButtonClick($(this), "bccname", "bccemail");
}

function onAddButtonClick(ctx, nameClass, emailClass) {
    var id1 = "dynamic" + nextID + "a";
    var id2 = "dynamic" + nextID + "b";
    var id3 = "dynamic" + nextID + "c";
    nextID++;

    ctx.closest('fieldset').append(
        "<input id='" + id1 + "' placeholder='Name' type='text' class='dynamic name " + nameClass + "'> ",
        "<input id='" + id2 + "' placeholder='Email address' type='email' class='dynamic email " + emailClass + "' required> ",
        "<button id='" + id3 + "' class='dynamic deleteBtn' onclick='onDeleteButtonClick(\"#" + id1 + "\", \"#" + id2 + "\", \"#" + id3 + "\")'> ");
}

function onDeleteButtonClick(id1, id2, id3) {
    $(id1).remove();
    $(id2).remove();
    $(id3).remove();
}

function resetForm() {
    // Remove all dynamically added elements
    $(".dynamic").remove();

    // Reset text fields to empty
    $(this).closest('form').find("input.name, input.email, textarea").val("");
}

function onSubmit(event) {
    event.preventDefault();

    // Map form data to javascript Object / JSON element
    var username = $("#username").val();
    var password = $("#password").val();
    var provider = $("#provider").val();
    var message = {
        from: toEmail($("#fromname").val(), $("#fromemail").val()),
        to: toEmailArray($(".toname"), $(".toemail")),
        cc: toEmailArray($(".ccname"), $(".ccemail")),
        bcc: toEmailArray($(".bccname"), $(".bccemail")),
        subject: $("#subject")[0].value,
        body: $("#body")[0].value
    };
    var providerParam = "";
    if (provider) {
        providerParam = "?EmailProvider=" + provider;
    }

    //console.log("onSubmit(): message: " + JSON.stringify(message));

    // POST request to service
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function (xhr) {
            xhr.setRequestHeader ("Authorization", "Basic " + btoa(username + ":" + password));
        },
        data: JSON.stringify(message),
        url: "/rest/mail" + providerParam,
        success: function(data){
            alert("Your message got the ID: " + data.id);
        },
        error: function(xhr, textStatus, errorMsg) {
            var msg = "";
            if (xhr.responseJSON && xhr.responseJSON.message) {
                alert("Got an error (status  " + xhr.status + ")\n" + xhr.responseJSON.message);
            } else {
                alert("Got an error (status  " + xhr.status + ")");
            }
        }
    });
}

function toEmailArray(nameElementArray, emailElementArray) {
    array = [];
    var i;
    for (i=0; i<emailElementArray.length; i++) {
        var email = toEmail(nameElementArray[i].value, emailElementArray[i].value);
        if (email) {
            array[i] = email;
        }
    }
    if (array.length == 0)
        return undefined;
    return array;
}

function toEmail(name, address) {
    var email = undefined;
    if (name && address) {
        email = {"name":name, "address": address};
    } else if (address) {
        email = {"address": address};
    }

    //console.log("Email: " + JSON.stringify(email));
    return email;
}

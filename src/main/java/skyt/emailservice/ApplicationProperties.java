package skyt.emailservice;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ApplicationProperties {

    // ********************************************************************************
    // Validation settings

    /**
     * The max size of the email body.
     * A value <=0 means unlimited. Default is 0 (i.e. unlimited).
     */
    @Value("${validation.maxBodyLength:0}")
    public int validation_maxBodyLength;

    /**
     * The max size of the subject.
     * A value <=0 means unlimited. Default is 0 (i.e. unlimited).
     */
    @Value("${validation.maxTotalSize:0}")
    public int validation_maxSubjectLength;

    /**
     * Max size of the total message, incl. everything.
     * A value <=0 means unlimited. Default is 9 MB.
     * One email provider has a max of 10 MB. Calculation of the size can differ.
     */
    @Value("${validation.maxTotalSize:9437184}")
    public int validation_maxTotalSize;

    /**
     * The maximum number of recipients.
     * A value <=0 means unlimited. Default value is 1000, which is also the max of at least one email provider.
     */
    @Value("${validation.maxRecipients:1000}")
    public int validation_maxRecipients;

    /**
     * List of whitelisted sender domains.
     */
    @Value("${validation.whitelistedSenderDomains:skytconsulting.dk}")
    public List<String> validation_whitelistedSenderDomains;

    
    // ********************************************************************************
    // Connection timeout settings

    /**
     * Determines the timeout in milliseconds until a connection is established.
     * A timeout value of zero is interpreted as an infinite timeout.
     * A negative value is interpreted as undefined (system default).
     * If not specified, the default value is 3000.
     */
    @Value("${httpclient.connectTimeout.milliseconds:3000}")
    public int connectTimeout;

    /**
     * The timeout in milliseconds used when requesting a connection from the connection manager.
     * A timeout value of zero is interpreted as an infinite timeout.
     * A negative value is interpreted as undefined (system default).
     * If not specified, the default value is 5000.
     */
    @Value("${httpclient.connectionRequestTimeout.milliseconds:5000}")
    public int connectionRequestTimeout;

    /**
     * Defines the socket timeout ({@code SO_TIMEOUT}) in milliseconds, which is the timeout for waiting for data or,
     * put differently, a maximum period inactivity between two consecutive data packets).
     * A timeout value of zero is interpreted as an infinite timeout.
     * A negative value is interpreted as undefined (system default).
     * If not specified, the default value is 3000.
     */
    @Value("${httpclient.socketTimeout.milliseconds:3000}")
    public int socketTimeout;


    // ********************************************************************************
    // Circuit breaker settings

    /** Set threshold for closing, e.g. close if 3 of the last 10 executions failed. */
    @Value("${circuitBreaker.failureThreshold.failures:3}")
    public int circuitBreaker_failureThreshold_failures;

    /** Set threshold for closing, e.g. close if 3 of the last 10 executions failed. */
    @Value("${circuitBreaker.failureThreshold.executions:10}")
    public int circuitBreaker_failureThreshold_executions;

    /** Set threshold for opening, e.g. after 5 successful trial executions. */
    @Value("${circuitBreaker.successThreshold:5}")
    public int circuitBreaker_successThreshold;

    /** After opening, a breaker will delay for, e.g. 1 minute, before before attempting to close again. */
    @Value("${circuitBreaker.delayMinutes:1}")
    public int circuitBreaker_delayMinutes;


    // ********************************************************************************
    // JobScheduler settings

    /**
     * The minimum number of threads to keep in the thread pool for sending emails.
     * Default is 5.
     */
    @Value("${jobscheduler.threadpool.corePoolSize:5}")
    public int jobscheduler_threadpool_corePoolSize;

    /**
     * When the scheduler looks for messages that should have been sent, be still have status QUEUED.
     * The value determines how long time the initial/previous processing should have to finish sending.
     * Default is 60 (seconds).
     */
    @Value("${jobscheduler.delayInSecondsBeforeRescheduling:60}")
    public long jobscheduler_delayInSecondsBeforeRescheduling;


    // ********************************************************************************
    // SendGrid (EmailProvider)

    @Value("${provider.sendgrid.apikey}")
    public String sendgrid_API_key;


    // ********************************************************************************
    // Mailgun (EmailProvider)

    @Value("${provider.mailgun.apikey}")
    public String mailgun_API_Key;

    @Value("${provider.mailgun.domainName:mailgun.skytconsulting.dk}")
    public String mailgun_DomainName;


    // ********************************************************************************
    // Sparkpost (EmailProvider)

    @Value("${provider.sparkpost.apikey}")
    public String sparkpost_API_key;


    // ********************************************************************************
    // Amazon Simple Email Service (EmailProvider)

    @Value("${provider.amazon-ses.region:eu-west-1}")
    public String aws_Region;

    @Value("${provider.amazon-ses.accessKeyId}")
    public String aws_AccessKeyId;

    @Value("${provider.amazon-ses.secretKey}")
    public String aws_SecretKey;

}

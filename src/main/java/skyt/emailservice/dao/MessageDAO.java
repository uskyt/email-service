package skyt.emailservice.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import skyt.emailservice.vo.Message;
import skyt.emailservice.vo.MessageHolder;

import java.io.IOException;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.Collections;
import java.util.List;

@Component
public class MessageDAO {
    private static final Logger log = LogManager.getLogger(MessageDAO.class);
    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static final int READ_LIMIT = 50;

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    private static final RowMapper<MessageHolder> rowMapper = (ResultSet rs, int rowNumber) -> {
        try {
            String jsonMessage = rs.getString("JSONMessage");
            Message message = objectMapper.readValue(jsonMessage, Message.class);

            MessageHolder.Status status = MessageHolder.Status.valueOf(rs.getString("Status"));

            MessageHolder result = new MessageHolder(
                    rs.getString("UUID"),
                    rs.getTimestamp("Created").toInstant(),
                    status,
                    message,
                    rs.getTimestamp("Modified").toInstant(),
                    rs.getString("ProviderID"),
                    rs.getString("MessageID"));

            return result;
        } catch (IOException e) {
            throw new SQLException("Error parsing JSON message", e);
        }
    };


    private static final String CREATE_SQL =
            "INSERT INTO Messages SET "
            + " UUID = :UUID, "
            + " Status = :Status, "
            + " Created = :Created, "
            + " Modified = :Modified, "
            + " JSONMessage = :JSONMessage ";

    public void createMessage(MessageHolder messageHolder) {
        String jsonMessage;
        try {
            jsonMessage = objectMapper.writeValueAsString(messageHolder.getMessage());
        } catch (JsonProcessingException e) {
            // This should never happen, serializing JSON should always be possible, since JSON was parsed from input
            throw new RuntimeException("Error serializing JSON to database", e);
        }

        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("UUID", messageHolder.getUuid())
                .addValue("Status", messageHolder.getStatus().name())
                .addValue("Created", messageHolder.getCreated())
                .addValue("Modified", messageHolder.getModified())
                .addValue("RequestedProviderID", messageHolder.getRequestedProviderID())
                .addValue("JSONMessage", jsonMessage);

        jdbcTemplate.update(CREATE_SQL, params);
    }


    private static final String READ_SQL = "SELECT * FROM Messages WHERE UUID = :UUID";

    public MessageHolder readMessageByUuid(String uuid) {

        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("UUID", uuid);

        MessageHolder messageHolder = jdbcTemplate.queryForObject(READ_SQL, params, rowMapper);

        return messageHolder;
    }

    private static final String UPDATE_MESSAGE_SQL =
            "UPDATE Messages " +
                    "SET " +
                    "  Status = :Status, " +
                    "  Modified = :Modified, " +
                    "  ProviderID = :ProviderID, " +
                    "  MessageID = :MessageID " +
                    "WHERE UUID = :UUID";

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateMessage(String uuid, MessageHolder.Status status, String providerID, String messageID) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("Modified", Date.from(Instant.now()))
                .addValue("Status", status.name())
                .addValue("ProviderID", providerID)
                .addValue("MessageID", messageID)
                .addValue("UUID", uuid);

        int rowsUpdated = jdbcTemplate.update(UPDATE_MESSAGE_SQL, params);

        if (rowsUpdated != 1) {
            log.error("setStatus expected to update one row, but rowsUpdated={}", rowsUpdated);
        }
    }

    private static final String ALLOCATE_OLD_MESSAGES_SQL =
            "UPDATE Messages " +
                    "SET HandlingProcessUUID = :HandlingProcessUUID, Modified = :Modified " +
                    "WHERE Status = 'QUEUED' " +
                    "  AND Modified < :ModifiedBefore " +
                    "ORDER BY Modified ASC " +
                    "LIMIT " + READ_LIMIT;

    private static final String READ_OLD_MESSAGES_SQL =
            "SELECT * FROM Messages " +
                    "WHERE Status = 'QUEUED' " +
                    "  AND Modified <= :Modified " + // Not a mistake - when we read, Modified has been updated. But we need to use the index.
                    "  AND HandlingProcessUUID = :HandlingProcessUUID " +
                    "ORDER BY Modified ASC ";

    /**
     * This method allocates a number of messages to be handled by the given handlingProcessID and returns these.
     * The way this is done in a transactional update and subsequent read protects against race conditions between
     * different processes doing this simultaneously.
     *
     * @param modifiedBefore Only allocate messages where Modified is older than this timestamp.
     * @param handlingProcessUUID A UUID identififying the process that is calling.
     * @return
     */
    @Transactional
    public List<MessageHolder> searchAndAllocateOldMessagesWithStatusQueued(Instant modifiedBefore, String handlingProcessUUID) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("Modified", Date.from(Instant.now()))
                .addValue("HandlingProcessUUID", handlingProcessUUID)
                .addValue("ModifiedBefore", Date.from(modifiedBefore));

        int rowsUpdated = jdbcTemplate.update(ALLOCATE_OLD_MESSAGES_SQL, params);

        if (rowsUpdated > 0) {
            List<MessageHolder> messages = jdbcTemplate.query(READ_OLD_MESSAGES_SQL, params, rowMapper);
            return messages;
        }

        return Collections.emptyList();
    }

    // For testing only!
    public NamedParameterJdbcTemplate setJdbcTemplate(NamedParameterJdbcTemplate jdbcTemplate) {
        NamedParameterJdbcTemplate old = this.jdbcTemplate;
        this.jdbcTemplate = jdbcTemplate;
        return old;
    }
}

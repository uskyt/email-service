package skyt.emailservice.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Component;
import skyt.emailservice.vo.User;

import java.sql.ResultSet;
import java.time.Instant;

@Component
public class UserDAO {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    private static final RowMapper<User> rowMapper = (ResultSet rs, int rowNumber) ->
            new User(rs.getLong("id"),
                    rs.getString("Username"),
                    rs.getString("PasswordHash"),
                    rs.getString("Salt"),
                    rs.getString("Email"),
                    rs.getString("FirstName"),
                    rs.getString("LastName"));


    private static final String GET_USER_BY_USERNAME_SQL =
            "SELECT * FROM Users WHERE Username = :Username";

    public User getUserByUsername(String username) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("Username", username);

        return jdbcTemplate.queryForObject(GET_USER_BY_USERNAME_SQL, params, rowMapper);
    }


    private static final String INC_LAST_USED_SQL =
            "UPDATE Users " +
                    "SET LastUsedTimestamp = :LastUsedTimestamp, " +
                    "    AuthenticatedRequestsCount = AuthenticatedRequestsCount+1 " +
                    "WHERE id = :id ";

    public void incrementRequestsCount(long id, Instant timestamp) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("id", id)
                .addValue("LastUsedTimestamp", timestamp);

        int rowsUpdated = jdbcTemplate.update(INC_LAST_USED_SQL, params);
    }


    private static final String INC_AUTH_FAILED_SQL =
            "UPDATE Users " +
                    "SET AuthenticationFailedCount = AuthenticationFailedCount+1 " +
                    "WHERE id = :id ";

    public void incrementAuthFailedCount(long id) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("id", id);

        int rowsUpdated = jdbcTemplate.update(INC_AUTH_FAILED_SQL, params);
    }
}

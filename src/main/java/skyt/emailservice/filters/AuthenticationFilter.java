package skyt.emailservice.filters;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import skyt.emailservice.service.AuthenticationService;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * The responsibility of this class is to authenticate requests to the REST service and reject unauthorized requests.
 *
 * This class is not annotated with @Component, because then there is no way to control the UrlPattern.
 * Instead, a FilterRegistrationBean is created in the EmailServiceApplication class.
 */
public class AuthenticationFilter implements Filter {
    private static final Logger log = LogManager.getLogger(AuthenticationFilter.class);

    private AuthenticationService authService;

    public AuthenticationFilter(AuthenticationService authService) {
        this.authService = authService;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        String authHeader = req.getHeader("Authorization");
        final String[] values = decodeBasicAuth(authHeader);

        if (values != null && values.length == 2) {
            // If the user is unknown or the password is wrong, this is logged in AuthenticationService
            boolean correct = authService.isPasswordCorrect(values[0], values[1]);
            if (correct) {
                filterChain.doFilter(request, response);
                return;
            }
            resp.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Invalid username or password");
        } else {
            log.warn("No valid Authorization header");
            resp.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Invalid Authorization header");
        }
    }

    private String[] decodeBasicAuth(String authHeader) {
        if (authHeader != null && authHeader.toLowerCase().startsWith("basic")) {
            // Authorization: Basic base64credentials
            String base64Credentials = authHeader.substring("Basic".length()).trim();
            byte[] credDecoded = Base64.getDecoder().decode(base64Credentials);
            String credentials = new String(credDecoded, StandardCharsets.UTF_8);
            // credentials = username:password
            return credentials.split(":", 2);
        }
        return null;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // Nothing to do
    }

    @Override
    public void destroy() {
        // Nothing to do
    }
}

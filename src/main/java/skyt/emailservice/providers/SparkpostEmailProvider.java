package skyt.emailservice.providers;

import com.sparkpost.Client;
import com.sparkpost.exception.SparkPostException;
import com.sparkpost.model.AddressAttributes;
import com.sparkpost.model.RecipientAttributes;
import com.sparkpost.model.TemplateContentAttributes;
import com.sparkpost.model.TransmissionWithRecipientArray;
import com.sparkpost.model.responses.Response;
import com.sparkpost.model.responses.TransmissionCreateResponse;
import com.sparkpost.resources.ResourceTransmissions;
import com.sparkpost.transport.IRestConnection;
import com.sparkpost.transport.RestConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import skyt.emailservice.ApplicationProperties;
import skyt.emailservice.vo.Email;
import skyt.emailservice.vo.Message;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * EmailProvider based on Sparkpost.
 */
@Component
public class SparkpostEmailProvider implements EmailProvider {
    private static final Logger log = LogManager.getLogger(SparkpostEmailProvider.class);
    public static final String PROVIDER_ID = "sparkpost.com";

    private Client client;

    @Autowired
    private ApplicationProperties props;

    @PostConstruct
    private void postConstruct() {
        // To use the SparkPost EU use the IRestConnection.SPC_EU_ENDPOINT endpoint
        client = new Client(props.sparkpost_API_key, IRestConnection.SPC_EU_ENDPOINT);
        client.setHttpConnectTimeout(props.connectTimeout);
        client.setHttpReadTimeout(props.socketTimeout);
    }


    @Override
    public String getProviderID() {
        return PROVIDER_ID;
    }

    @Override
    public String sendEmail(Message message) {
        try {
            TransmissionWithRecipientArray transmission = new TransmissionWithRecipientArray();

            // Populate Recipients
            List<RecipientAttributes> recipientArray = new ArrayList<>();
            for (Email recipient : message.getTo()) {
                RecipientAttributes recipientAttribs = new RecipientAttributes();
                recipientAttribs.setAddress(mapEmail(recipient));
                recipientArray.add(recipientAttribs);
            }
            for (Email recipient : message.getCc()) {
                RecipientAttributes recipientAttribs = new RecipientAttributes();
                recipientAttribs.setAddress(mapEmail(recipient));
                recipientArray.add(recipientAttribs);
            }
            for (Email recipient : message.getBcc()) {
                RecipientAttributes recipientAttribs = new RecipientAttributes();
                recipientAttribs.setAddress(mapEmail(recipient));
                recipientArray.add(recipientAttribs);
            }
            transmission.setRecipientArray(recipientArray);

            // Populate Email Body
            TemplateContentAttributes contentAttributes = new TemplateContentAttributes();
            contentAttributes.setFrom(mapEmail(message.getFrom()));
            contentAttributes.setSubject(message.getSubject());
            contentAttributes.setText(message.getBody());
            transmission.setContentAttributes(contentAttributes);

            // Send the Email
            RestConnection connection = new RestConnection(client, IRestConnection.SPC_EU_ENDPOINT);
            Response response = ResourceTransmissions.create(connection, 0, transmission);

            if (response.getResponseCode() == 200) {
                log.debug("Transmission Response: " + response);
                return ((TransmissionCreateResponse) response).getResults().getId();
            } else {
                log.error("Error from Sparkpost, Transmission Response: " + response);
                throw new RuntimeException("Unexpected ResponseCode=" + response.getResponseCode() + " from EmailProvider=Sparkpost");
            }
        } catch (SparkPostException e) {
            throw new RuntimeException("Error sending mail via SparkPost", e);
        }
    }

    private AddressAttributes mapEmail(Email email) {
        return new AddressAttributes(email.getAddress(), email.getName(), null);
    }

//   https://api.eu.sparkpost.com/api/v1/transmissions \
//           -H "Authorization: 474a8149fc61f2bfdc36ac31bfb5e67f1e7ea19c" \
//           -H "Content-Type: application/json" \
//           -d '{
//             "options": {
//               "sandbox": true
//             },
//             "content": {
//               "from": "sandbox@sparkpostbox.com",
//               "subject": "Thundercats are GO!!!",
//               "text": "Sword of Omens, give me sight BEYOND sight"
//             },
//             "recipients": [{ "address": "ulrik.skyt@gmail.com" }]
//         }'
//        {"results":{"total_rejected_recipients":0,"total_accepted_recipients":1,"id":"6653475387472544620"}}
}

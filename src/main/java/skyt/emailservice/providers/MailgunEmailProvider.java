package skyt.emailservice.providers;

import org.apache.commons.io.IOUtils;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import skyt.emailservice.ApplicationProperties;
import skyt.emailservice.vo.Email;
import skyt.emailservice.vo.Message;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * EmailProvider implementation based on Mailgun.
 *
 * @See http://www.mailgun.com/
 * @See http://documentation.mailgun.com/quickstart.html#sending-messages
 */
@Component
public class MailgunEmailProvider implements EmailProvider {
    private static final Logger log = LogManager.getLogger(MailgunEmailProvider.class);
    public static final String PROVIDER_ID = "mailgun.com";

    private String url;

    @Autowired
    private ApplicationProperties props;

    @Autowired
    private CloseableHttpClient httpClient;


    @PostConstruct
    private void postConstruct() {
        url = "https://api.eu.mailgun.net/v3/" + props.mailgun_DomainName + "/messages";
    }

    @Override
    public String getProviderID() {
        return PROVIDER_ID;
    }

    @Override
    public String sendEmail(Message mail) {
        try {
            HttpPost httpPost = setupRequest(mail);
            String responseStr = null;

            try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
                int statusCode = response.getStatusLine().getStatusCode();
                switch (statusCode) {
                    case 200: // Everything worked as expected
                        responseStr = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
                        log.debug("Message sent successfully with Mailgun, response: {}", responseStr);

                        JSONParser parser = new JSONParser();
                        JSONObject jsonObject = (JSONObject) parser.parse(responseStr);

                        String message = (String) jsonObject.get("message");
                        String id = (String) jsonObject.get("id");
                        return id;
                    case 400: // Bad Request - Often missing a required parameter
                    case 401: // Unauthorized - No valid API key provided
                    case 402: // Request Failed - Parameters were valid but request failed
                    case 404: // Not Found - The requested item doesn’t exist
                    case 413: // Request Entity Too Large - Attachment size is too big
                    default:  // 500, 502, 503, 504: Server Errors - something is wrong on Mailgun’s end
                        String encoding = response.getEntity().getContentEncoding() == null ? "UTF8" : response.getEntity().getContentEncoding().getValue();
                        String body = IOUtils.toString(response.getEntity().getContent(), encoding);
                        log.error("Error from Mailgun, status={} body: {}", statusCode, body);
                        throw new RuntimeException("Unexpected StatusCode=" + statusCode + " from MailProvider=Mailgun");
                }
            } catch (ParseException e) {
                // Dilemma: Fail with exception or log and return silently?
                // The email was sent successfully, so I choose to return silently.
                // The only consequence is that the proper MessageID will not be saved in the database.
                log.error("Error parsing JSON body from success response, responseStr={}", responseStr, e);
                return null;
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private HttpPost setupRequest(Message message) throws UnsupportedEncodingException {
        HttpPost httpPost = new HttpPost(url);
        try {
            UsernamePasswordCredentials credentials
                    = new UsernamePasswordCredentials("api", props.mailgun_API_Key);
            httpPost.addHeader(new BasicScheme().authenticate(credentials, httpPost, null));
        } catch (AuthenticationException e) {
            // This should never happen
            throw new RuntimeException("Error setting up BasicAuth", e);
        }

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("from", message.getFrom().toString()));
        for (Email to : message.getTo()) {
            params.add(new BasicNameValuePair("to", to.toString()));
        }
        for (Email cc : message.getCc()) {
            params.add(new BasicNameValuePair("cc", cc.toString()));
        }
        for (Email bcc : message.getBcc()) {
            params.add(new BasicNameValuePair("bcc", bcc.toString()));
        }
        if (message.getSubject() != null) {
            params.add(new BasicNameValuePair("subject", message.getSubject()));
        }
        if (message.getBody() != null) {
            params.add(new BasicNameValuePair("text", message.getBody()));
        }

        httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF8"));
        return httpPost;
    }
}

package skyt.emailservice.providers;

import com.sendgrid.*;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import skyt.emailservice.ApplicationProperties;
import skyt.emailservice.vo.Message;

import javax.annotation.PostConstruct;
import java.io.IOException;

/**
 * EmailProvider implementation based on SendGrid.
 *
 * @See https://sendgrid.com/user/signup
 * @See https://sendgrid.com/docs/API_Reference/Web_API/mail.html
 * @See https://sendgrid.com/docs/API_Reference/Web_API_v3/Mail/index.html
 * @See https://app.sendgrid.com/guide/integrate/langs/java
 */
@Component
public class SendGridEmailProvider implements EmailProvider {
    private static final Logger log = LogManager.getLogger(SendGridEmailProvider.class);
    public static final String PROVIDER_ID = "sendgrid.com";


    /**
     * API class from the SendGrid library.
     * Allow protected access in order to set a different host for testing on a mock service.
     */
    protected SendGrid sendGrid;

    /**
     * SendGrid 'Client' which wraps an Apache HttpClient, which we must give to control its timeout settings.
     * Allow protected access in order to set 'test' mode from unit tests (allowing http rather than https).
     */
    protected Client client;

    @Autowired
    private CloseableHttpClient httpClient;

    @Autowired
    private ApplicationProperties props;

    @PostConstruct
    private void postConstruct() {
        client = new Client(httpClient);
        sendGrid = new SendGrid(props.sendgrid_API_key, client);
    }

    @Override
    public String getProviderID() {
        return PROVIDER_ID;
    }

    @Override
    public String sendEmail(Message message) {
        Mail sgMail = mapMail(message);
        return send(sgMail);
    }

    private String send(Mail mail) {
        try {
            Request request = new Request();
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sendGrid.api(request);

            if (response.getStatusCode() == 200 || response.getStatusCode() == 202) {
                log.debug("Response from sendEmail: statusCode={}, body={}, headers={}",
                        response.getStatusCode(), response.getBody(), response.getHeaders());
                String messageID = response.getHeaders().get("X-Message-Id");
                return messageID;
            } else {
                // See: https://sendgrid.com/docs/API_Reference/Web_API_v3/Mail/errors.html
                log.error("Error from SendGrid, status={} body={} headers: {}",
                        response.getStatusCode(), response.getBody(), response.getHeaders());
                throw new RuntimeException("Unexpected StatusCode=" + response.getStatusCode() + " from EmailProvider=SendGrid");
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Mail mapMail(Message in) {
        Mail out = new Mail();

        out.setFrom(mapEmail(in.getFrom()));
        out.setSubject(in.getSubject());

        Personalization personalization = new Personalization();
        for (skyt.emailservice.vo.Email to : in.getTo()) {
            personalization.addTo(mapEmail(to));
        }
        for (skyt.emailservice.vo.Email cc : in.getCc()) {
            personalization.addTo(mapEmail(cc));
        }
        for (skyt.emailservice.vo.Email bcc : in.getBcc()) {
            personalization.addTo(mapEmail(bcc));
        }
        out.addPersonalization(personalization);


        Content content = new Content("text/plain", in.getBody());
        out.addContent(content);

        return out;
    }

    private Email mapEmail(skyt.emailservice.vo.Email email) {
        return new Email(email.getAddress(), email.getName());
    }
}

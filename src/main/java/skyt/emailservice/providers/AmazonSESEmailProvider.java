package skyt.emailservice.providers;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import skyt.emailservice.ApplicationProperties;
import skyt.emailservice.vo.Email;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;


/**
 * EmailProvider based on Amazon Simple Email Service (SES).
 */
@Component
public class AmazonSESEmailProvider implements EmailProvider {
    private static final Logger log = LogManager.getLogger(AmazonSESEmailProvider.class);
    public static final String PROVIDER_ID = "amazon-ses";

    private AmazonSimpleEmailService client;

    @Autowired
    private ApplicationProperties props;

    @PostConstruct
    private void postConstruct() {
        ClientConfiguration config = new ClientConfiguration()
                .withConnectionTimeout(props.connectTimeout)
                .withRequestTimeout(props.connectionRequestTimeout)
                .withSocketTimeout(props.socketTimeout);

        client = AmazonSimpleEmailServiceClientBuilder.standard()
                .withRegion(Regions.fromName(props.aws_Region))
                .withCredentials(new AWSStaticCredentialsProvider(
                        new BasicAWSCredentials(props.aws_AccessKeyId, props.aws_SecretKey)))
                .withClientConfiguration(config)
                .build();
    }

    @Override
    public String getProviderID() {
        return PROVIDER_ID;
    }

    // This method throws misc. AmazonSimpleEmailServiceExceptions which are RuntimeExceptions.
    // They will be logged elsewhere.
    @Override
    public String sendEmail(skyt.emailservice.vo.Message message) {
        SendEmailRequest request = new SendEmailRequest()
                .withDestination(new Destination()
                        .withToAddresses(mapEmails(message.getTo()))
                        .withCcAddresses(mapEmails(message.getCc()))
                        .withBccAddresses(mapEmails(message.getBcc())))
                .withMessage(new com.amazonaws.services.simpleemail.model.Message()
                        .withBody(new Body()
//                                    .withHtml(new Content()
//                                            .withCharset("UTF-8").withData(HTMLBODY))
                                .withText(new Content()
                                        .withCharset("UTF-8").withData(message.getBody())))
                        .withSubject(new Content()
                                .withCharset("UTF-8").withData(message.getSubject())))
                .withSource(message.getFrom().toString());
                // Comment or remove the next line if you are not using a
                // configuration set
                //.withConfigurationSetName(CONFIGSET);
        SendEmailResult sendEmailResult = client.sendEmail(request);
        log.debug("Message sent successfully with Amazon SES, response: {}", sendEmailResult);
        return sendEmailResult.getMessageId();
    }

    private Collection<String> mapEmails(Email[] emails) {
        return Arrays.stream(emails)
                .map(e -> e.toString())
                .collect(Collectors.toList());
    }
}

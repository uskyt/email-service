package skyt.emailservice.providers;

import skyt.emailservice.vo.Message;

/**
 * Generic interface to an Email Provider.
 * Specific Email Providers should implement this interface.
 */
public interface EmailProvider {
    /**
     * Identifying string of the EmailProvider. Max length is 30.
     */
    String getProviderID();

    /**
     * Sends the given message.
     *
     * @param message
     * @return MessageID from EmailProvider.
     */
    String sendEmail(Message message);
}

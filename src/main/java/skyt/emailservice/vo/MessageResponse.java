package skyt.emailservice.vo;

public class MessageResponse {
    private String id;

    public MessageResponse() {}

    public MessageResponse(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

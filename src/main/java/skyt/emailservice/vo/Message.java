package skyt.emailservice.vo;

import java.util.Arrays;

/**
 * Value Object representing the mail to be sent.
 * This class is used to deserialize the JSON input given to the REST service.
 *
 * NOTE: It is also used to serialize/deserialize messages to a BLOB field in the database.
 * So only change with considerable carefulness!
 */
public class Message {
    // Mandatory field.
    private Email from;

    // Mandatory field.
    private Email[] to;

    // Default to an empty array, to avoid handling null-array.
    private Email[] cc = {};

    // Default to an empty array, to avoid handling null-array.
    private Email[] bcc = {};

    private String subject;

    private String body;


    public Email getFrom() {
        return from;
    }

    public void setFrom(Email from) {
        this.from = from;
    }

    public Email[] getTo() {
        return to;
    }

    public void setTo(Email[] to) {
        this.to = to;
    }

    public Email[] getCc() {
        return cc;
    }

    public void setCc(Email[] cc) {
        this.cc = cc;
    }

    public Email[] getBcc() {
        return bcc;
    }

    public void setBcc(Email[] bcc) {
        this.bcc = bcc;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "Message{" +
                "from='" + from + '\'' +
                ", to=" + Arrays.toString(to) +
                ", cc=" + Arrays.toString(cc) +
                ", bcc=" + Arrays.toString(bcc) +
                ", subject='" + subject + '\'' +
                ", body='" + body + '\'' +
                '}';
    }

    /**
     * NOTE: This method is used to test for equality in unit tests
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Message)) return false;

        Message message = (Message) o;

        if (from != null ? !from.equals(message.from) : message.from != null) return false;
        if (!Arrays.equals(to, message.to)) return false;
        if (!Arrays.equals(cc, message.cc)) return false;
        if (!Arrays.equals(bcc, message.bcc)) return false;
        if (subject != null ? !subject.equals(message.subject) : message.subject != null) return false;
        return body != null ? body.equals(message.body) : message.body == null;
    }
}

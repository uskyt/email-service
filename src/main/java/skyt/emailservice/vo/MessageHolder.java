package skyt.emailservice.vo;

import java.time.Instant;
import java.util.UUID;

/**
 * This class represents a Message as it is saved in the database.
 */
public class MessageHolder {
    private String requestedProviderID;

    public enum Status {
        REJECTED,          // Rejected - client got a validation error
        QUEUED,            // Accepted and queued, not sent yet
        SENT,              // Sent successfully
        ERROR_NOT_HANDLED, // Could not be sent - an error-email should be sent to sender
        ERROR_HANDLED;     // Error has been handled
    }

    private String uuid;
    private Instant created;
    private Status status;
    private Message message;

    private Instant modified;
    private String providerID;
    private String MessageID;

    public MessageHolder(Message message) {
        this(message, null);
    }

    public MessageHolder(Message message, String requestedProviderID) {
        this.uuid = UUID.randomUUID().toString();
        this.created = Instant.now();
        this.modified = created;
        this.message = message;
        this.status = Status.QUEUED;
        this.requestedProviderID = requestedProviderID;
    }

    public MessageHolder(String uuid, Instant created, Status status, Message message, Instant modified,
                         String providerID, String messageID) {
        this.uuid = uuid;
        this.created = created;
        this.status = status;
        this.message = message;
        this.modified = modified;
        this.providerID = providerID;
        MessageID = messageID;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Instant getCreated() {
        return created;
    }

    public void setCreated(Instant created) {
        this.created = created;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public Instant getModified() {
        return modified;
    }

    public void setModified(Instant modified) {
        this.modified = modified;
    }

    public String getRequestedProviderID() {
        return requestedProviderID;
    }

    public void setRequestedProviderID(String requestedProviderID) {
        this.requestedProviderID = requestedProviderID;
    }

    public String getProviderID() {
        return providerID;
    }

    public void setProviderID(String providerID) {
        this.providerID = providerID;
    }

    public String getMessageID() {
        return MessageID;
    }

    public void setMessageID(String messageID) {
        MessageID = messageID;
    }
}

package skyt.emailservice.vo;

/**
 * Value Object representating an optional full name and an email address.
 * This class is used to deserialize the JSON input given to the REST service.
 *
 * NOTE: It is also used to serialize/deserialize messages to a BLOB field in the database.
 * So only change with considerable carefulness!
 */
public class Email {
    /**
     * Full name, for example "John Doe".
     * The name is not mandatory, so this value can be null.
     */
    private String name;

    /**
     * Email address, for example "john.doe@example.com".
     */
    private String address;

    public Email() {}

    public Email(String address) {
        this.address = address;
    }

    public Email (String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * NOTE: The format of this toString() is used in code (e.g. {@link skyt.emailservice.providers.MailgunEmailProvider}).
     */
    public String toString() {
        if (name != null) {
            return name + " <" + address + ">";
        }
        return address;
    }

    /**
     * NOTE: This method is used to test for equality in unit tests
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Email)) return false;

        Email email = (Email) o;

        if (name != null ? !name.equals(email.name) : email.name != null) return false;
        return address != null ? address.equals(email.address) : email.address == null;
    }

    /**
     * @return the length of the String that the toString() method would return (without actually constructing it).
     */
    public int length() {
        if (name != null) {
            return name.length() + address.length() + 3;
        }
        return address.length();
    }
}

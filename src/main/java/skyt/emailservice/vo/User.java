package skyt.emailservice.vo;

public class User {
    private long id;
    private String username;
    private String passwordHash;
    private String salt;
    private String email;
    private String firstName;
    private String lastName;

    public User() {}

    public User(long id, String username, String passwordHash, String salt, String email, String firstName, String lastName) {
        this.id = id;
        this.username = username;
        this.passwordHash = passwordHash;
        this.salt = salt;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public String getSalt() {
        return salt;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}

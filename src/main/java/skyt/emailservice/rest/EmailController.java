package skyt.emailservice.rest;

import io.swagger.annotations.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import skyt.emailservice.providers.AmazonSESEmailProvider;
import skyt.emailservice.providers.MailgunEmailProvider;
import skyt.emailservice.providers.SendGridEmailProvider;
import skyt.emailservice.providers.SparkpostEmailProvider;
import skyt.emailservice.service.QueueEmailService;
import skyt.emailservice.vo.Message;
import skyt.emailservice.vo.MessageHolder;
import skyt.emailservice.vo.MessageResponse;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/rest")
@Api(value="Email Service", description="Service for sending email")
public class EmailController {
    private static final Logger log = LogManager.getLogger(EmailController.class);
    public static final String PARAM_EMAIL_PROVIDER = "EmailProvider";

    @Autowired
    private QueueEmailService queueEmailService;

    @ApiOperation(value = "Send an email")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Message queued successfully"),
            @ApiResponse(code = 400, message = "JSON parse error: ${details}"),
            @ApiResponse(code = 400, message = "Invalid email address '${email}' in the '${field}' field"),
            @ApiResponse(code = 400, message = "Too many recipients, current max is ${max}"),
            @ApiResponse(code = 400, message = "Sender domain not whitelisted: ${domain}"),
            @ApiResponse(code = 400, message = "Unknown EmailProvider: ${emailProvider}"),
            @ApiResponse(code = 401, message = "Invalid username or password"),
            @ApiResponse(code = 401, message = "Invalid Authorization header"),
            @ApiResponse(code = 413, message = "Message subject was too long"),
            @ApiResponse(code = 413, message = "Message body was too large"),
            @ApiResponse(code = 413, message = "Message total size was too large"),
            @ApiResponse(code = 500, message = "Internal error"),
            @ApiResponse(code = 503, message = "Service unavailable")
    })
    @RequestMapping(value="/mail", method=POST)
    public MessageResponse sendMail(@RequestBody Message message,
                                    @ApiParam(allowableValues = "amazon-ses, mailgun.com, sendgrid.com, sparkpost.com")
                                    @RequestParam(name=PARAM_EMAIL_PROVIDER, required=false) String emailProviderID) {
        log.debug("sendMail called with: " + message);
        MessageHolder messageHolder = queueEmailService.queueEmail(message, emailProviderID);
        return new MessageResponse(messageHolder.getUuid());
    }
}

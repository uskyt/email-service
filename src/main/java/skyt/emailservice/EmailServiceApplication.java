package skyt.emailservice;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import skyt.emailservice.filters.AuthenticationFilter;
import skyt.emailservice.providers.EmailProvider;
import skyt.emailservice.service.AuthenticationService;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.sql.DataSource;
import java.util.*;

@SpringBootApplication
@PropertySource(value={"application.properties", "secret.properties"}, ignoreResourceNotFound = true)
@EnableSwagger2
public class EmailServiceApplication {

    @Autowired
    private ApplicationProperties props;

    public static void main(String[] args) {
        SpringApplication.run(EmailServiceApplication.class, args);
    }

    @Bean
    public Docket api() {
        List<SecurityScheme> schemeList = new ArrayList<>();
        schemeList.add(new BasicAuth("basicAuth"));

        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
                .securitySchemes(schemeList)
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "REST API for email-service",
                "This is a backend service that accepts the necessary information for sending emails. " +
                        "It provides an abstraction between multiple different email service providers. " +
                        "If one of the service providers goes down, this service can quickly failover to a different provider without affecting its customers.",
                "1.0",
                null,
                new Contact("Ulrik Skyt", null, "ulrik.skyt@gmail.com"),
                "MIT License", "https://opensource.org/licenses/MIT", Collections.emptyList());
    }

    @Bean
    public CloseableHttpClient httpClient() {
        return HttpClientBuilder.create()
                .setDefaultRequestConfig(RequestConfig.custom()
                        .setConnectTimeout(props.connectTimeout)
                        .setConnectionRequestTimeout(props.connectionRequestTimeout)
                        .setSocketTimeout(props.socketTimeout)
                        .build())
                .useSystemProperties() // allow for standard environment variables to control this HttpClient
                .build();
    }

    @Bean
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate(DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }

    @Bean
    public Map<String, EmailProvider> emailProviderMap(List<EmailProvider> emailProviders) {
        Map<String, EmailProvider> map = new HashMap<>();
        for (EmailProvider p : emailProviders) {
            map.put(p.getProviderID(), p);
        }
        return map;
    }

    @Bean
    public FilterRegistrationBean<AuthenticationFilter> authenticationFilter(AuthenticationService authenticationService) {
        FilterRegistrationBean<AuthenticationFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new AuthenticationFilter(authenticationService));
        registrationBean.addUrlPatterns("/rest/*");
        registrationBean.setOrder(1);
        return registrationBean;
    }
}


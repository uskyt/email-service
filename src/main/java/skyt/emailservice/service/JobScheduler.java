package skyt.emailservice.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import skyt.emailservice.ApplicationProperties;
import skyt.emailservice.dao.MessageDAO;
import skyt.emailservice.vo.MessageHolder;

import javax.annotation.PreDestroy;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * This class manages a thread pool and schedules jobs to be run ASAP and one job to be run periodically.
 */
@Component
public class JobScheduler {
    private static final Logger log = LogManager.getLogger(JobScheduler.class);
    private final String processUUID = UUID.randomUUID().toString();

    @Autowired
    private MessageDAO messageDAO;

    @Autowired
    private ApplicationProperties properties;

    @Autowired
    private SendEmailService sendEmailService;

    private ScheduledExecutorService executor;

    @PreDestroy
    private void preDestroy() {
        executor.shutdown();
    }

    @Autowired
    public JobScheduler(ApplicationProperties properties) {
        executor = Executors.newScheduledThreadPool(properties.jobscheduler_threadpool_corePoolSize);
        executor.scheduleWithFixedDelay(() -> checkForUnsentMessages(), 5, 60, TimeUnit.SECONDS);
    }

    public void submitSendMessageJob(MessageHolder messageHolder) {
        log.debug("submitSendMessageJob");
        if (messageHolder.getStatus() != MessageHolder.Status.QUEUED) {
            throw new IllegalStateException("Expected message to have status QUEUED");
        }
        executor.execute(() -> sendEmailService.sendEmail(messageHolder));
    }


    private void checkForUnsentMessages() {
        log.debug("checkForUnsentMessages");

        // It's important to catch all Exceptions in this method, otherwise the scheduling stops
        try {
            List<MessageHolder> messageHolders;
            do {
                // Find messages with status QUEUED that should have been sent by now
                Instant modifiedBefore = Instant.now().minusSeconds(properties.jobscheduler_delayInSecondsBeforeRescheduling);
                messageHolders = messageDAO.searchAndAllocateOldMessagesWithStatusQueued(modifiedBefore, processUUID);

                log.info("checkForUnsentMessages found {} messages that should have been sent by now", messageHolders.size());

                // Schedule the message to be sent
                for (MessageHolder mh : messageHolders) {
                    executor.execute(() -> sendEmailService.sendEmail(mh));
                }
            } while (messageHolders.size() >= MessageDAO.READ_LIMIT);
        } catch (Exception e) {
            log.error("Exception from checkForUnsentMessages", e);
        }
    }
}

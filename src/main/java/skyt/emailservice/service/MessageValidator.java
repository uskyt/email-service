package skyt.emailservice.service;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import skyt.emailservice.ApplicationProperties;
import skyt.emailservice.providers.EmailProvider;
import skyt.emailservice.vo.Email;
import skyt.emailservice.vo.Message;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class MessageValidator {

    // Writing your own email address validator is surprisingly complex!
    // So we stick with an Apache implementation here...
    private static final EmailValidator validator = EmailValidator.getInstance();

    @Autowired
    private ApplicationProperties properties;

    @Autowired
    @Qualifier("emailProviderMap")
    private Map<String, EmailProvider> emailProviderMap;

    /**
     * This method validates:
     *
     *  - The format of all email adresses in the fields from, to, cc and bcc.
     *  - Body size (configurable limit)
     *  - Subject size (configurable limit)
     *  - Total size of message (configurable limit)
     *  - Number of recipients (configurable limit)
     *
     * @param message The message to be validated.
     * @throws ValidationException If email adresses have errors, or there there are too many recipients.
     * @throws MessageTooLargeException If one of the configured limits for a message is exceeded.
     */
    public void validateMessage(Message message) {
        validateEmail(message.getFrom(), "from");
        validateWhitelistedSenderDomain(message.getFrom().getAddress());

        for (Email email : message.getTo()) {
            validateEmail(email, "to");
        }
        for (Email email : message.getCc()) {
            validateEmail(email, "cc");
        }
        for (Email email : message.getBcc()) {
            validateEmail(email, "bcc");
        }

        validateBodySize(message.getBody());
        validateSubjectSize(message.getSubject());
        validateTotalSize(message);
        validateMaxRecipients(message);
    }

    public void validateRequestedProviderID(String requestedProviderID) {
        if (requestedProviderID != null && !emailProviderMap.containsKey(requestedProviderID)) {
            throw new ValidationException("Unknown EmailProvider: '" + requestedProviderID + "'");
        }
    }

    private void validateWhitelistedSenderDomain(String address) {
        // This depends on the address format being validated before this call
        String domain = address.substring(address.indexOf('@')+1).toLowerCase();
        if (!properties.validation_whitelistedSenderDomains.contains(domain)) {
            throw new ValidationException("Sender domain not whitelisted: " + domain);
        }
    }

    // package-private access, to allow unit testing.
    void validateEmail(Email email, String field) {
        validateEmailAddress(email.getAddress(), field);
    }

    // package-private access, to allow unit testing.
    void validateEmailAddress(String email, String field) {
        if (!validator.isValid(email)) {
            throw new ValidationException("Invalid email address '" + email + "' in the '" + field + "' field");
        }
    }

    // package-private access, to allow unit testing.
    void validateBodySize(String body) {
        // A configured max value <= 0 means unlimited.
        if (properties.validation_maxBodyLength > 0 && body.length() > properties.validation_maxBodyLength) {
            throw new MessageTooLargeException("Message body was too large");
        }
    }

    // package-private access, to allow unit testing.
    void validateSubjectSize(String subject) {
        // A configured max value <= 0 means unlimited.
        if (properties.validation_maxSubjectLength > 0 && subject.length() > properties.validation_maxSubjectLength) {
            throw new MessageTooLargeException("Message subject was too long");
        }
    }

    /**
     * This method calculates a rough estimate on the total size of the message.
     * Some mail providers have a limit on this.
     *
     * Amazon SES: 10 MB.
     * https://docs.aws.amazon.com/ses/latest/DeveloperGuide/limits.html
     *
     * SendGrid: 30 MB.
     * https://sendgrid.com/docs/API_Reference/Web_API_v3/Mail/index.html
     *
     * Sparkpost: (no documented limits found)
     *
     * Mailgun: (no documented limits found)
     *
     * @param message
     */
    // package-private access, to allow unit testing.
    void validateTotalSize(Message message) {
        // A configured max value <= 0 means unlimited.
        if (properties.validation_maxTotalSize <= 0) {
            return;
        }

        int total = 0;

        total += message.getFrom().length() + 8; // "FROM: " + CRLF

        total += 6; // "TO: " + CRLF
        for (Email email : message.getTo()) {
            total += email.length() + 2; // ", "
        }
        for (Email email : message.getCc()) {
            total += email.length() + 2; // ", "
        }
        for (Email email : message.getBcc()) {
            total += email.length() + 2; // ", "
        }

        total += message.getSubject().length() + 11; // "SUBJECT: " + CRLF
        total += message.getBody().length() + 2;

        if (total > properties.validation_maxTotalSize) {
            throw new MessageTooLargeException("Message total size was too large");
        }
    }

    // package-private access, to allow unit testing.
    void validateMaxRecipients(Message message) {
        // A configured max value <= 0 means unlimited.
        if (properties.validation_maxRecipients <= 0) {
            return;
        }

        int total = 0;

        total += message.getTo().length;
        total += message.getCc().length;
        total += message.getBcc().length;

        if (total > properties.validation_maxRecipients) {
            throw new ValidationException("Too many recipients, current max is " + properties.validation_maxRecipients);
        }
    }
}

package skyt.emailservice.service;

import net.jodah.failsafe.CircuitBreaker;
import net.jodah.failsafe.Failsafe;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import skyt.emailservice.ApplicationProperties;
import skyt.emailservice.dao.MessageDAO;
import skyt.emailservice.providers.EmailProvider;
import skyt.emailservice.vo.MessageHolder;

import java.time.Duration;
import java.util.*;

/**
 * When called with a Message (from a scheduled job), this class chooses an EmailProvider and its CircuitBreaker, and
 * use them to try to send the message. If it fails the other EmailProviders are tried. Ultimately the message is
 * marked with status SENT (and some more information) or ERROR_NOT_HANDLED.
 */
@Component
public class SendEmailService {
    private static final Logger log = LogManager.getLogger(SendEmailService.class);

    private List<EmailProvider> providers;

    @Autowired
    private MessageDAO messageDAO;


    @Autowired
    @Qualifier("emailProviderMap")
    private Map<String, EmailProvider> emailProviderMap;

    private Map<String, CircuitBreaker<Void>> circuitBreakerMap = new HashMap<>();

    @Autowired
    public SendEmailService(List<EmailProvider> providers, ApplicationProperties props) {
        this.providers = providers;

        // Configure a separate CircuitBreaker for each EmailProvider
        for (int i=0; i<providers.size(); i++) {
            CircuitBreaker<Void> circuitBreaker = new CircuitBreaker<Void>()
                    .withFailureThreshold(props.circuitBreaker_failureThreshold_failures,
                            props.circuitBreaker_failureThreshold_executions)
                    .withSuccessThreshold(props.circuitBreaker_successThreshold)
                    .withDelay(Duration.ofMinutes(props.circuitBreaker_delayMinutes));
            circuitBreakerMap.put(providers.get(i).getProviderID(), circuitBreaker);
        }
    }

    /**
     * Validate the given mail and then send it using one of the available email providers.
     *
     * A number of EmailProviders can be available to do the actual sending.
     * Calls to the implementing {@link EmailProvider} classes are protected using a {@link CircuitBreaker}.
     * In case of failure, try the next provider. If all fail, an error is logged and status is set to
     * ERROR_NOT_HANDLED in the database.
     *
     * @param messageHolder A MessageHolder object holding the message to send.
     */
    public void sendEmail(MessageHolder messageHolder) {
        if (messageHolder.getRequestedProviderID() == null) {
            // A specific EmailProvider has not been specified - use any of the available providers
            sendUsingProviderFromList(messageHolder, providers);
        } else {
            sendUsingSpecificEmailProvider(messageHolder);
        }
    }

    private void sendUsingSpecificEmailProvider(MessageHolder messageHolder) {
        // look up provider
        EmailProvider provider = emailProviderMap.get(messageHolder.getRequestedProviderID());
        if (provider == null) {
            log.error("Unknown EmailProvider '" + messageHolder.getRequestedProviderID() +
                    "', setting status ERROR_NOT_HANDLED");
            messageDAO.updateMessage(messageHolder.getUuid(), MessageHolder.Status.ERROR_NOT_HANDLED,
                    messageHolder.getProviderID(), messageHolder.getMessageID());
            return;
        }

        sendUsingProviderFromList(messageHolder, Collections.singletonList(provider));
    }

    private void sendUsingProviderFromList(MessageHolder messageHolder, List<EmailProvider> myProviders) {
        for (int i=0; i<myProviders.size(); i++) {
            EmailProvider provider = myProviders.get(i);
            CircuitBreaker<Void> circuitBreaker = circuitBreakerMap.get(provider.getProviderID());

            try {
                log.debug("Sending message with EmailProvider={}", provider.getClass().getSimpleName());
                Failsafe.with(circuitBreaker)
                        .run(() -> doSendEmail(provider, messageHolder));
                return;
            } catch (RuntimeException e) {
                log.error("Error sending mail with EmailProvider={}", provider.getClass().getSimpleName(), e);
            }
        }

        log.error("Error sending mail with all EmailProviders: " + messageHolder.getMessage());
        messageDAO.updateMessage(messageHolder.getUuid(), MessageHolder.Status.ERROR_NOT_HANDLED, null, null);
    }

    private void doSendEmail(EmailProvider provider, MessageHolder messageHolder) {
        String messageID = provider.sendEmail(messageHolder.getMessage());
        messageDAO.updateMessage(messageHolder.getUuid(), MessageHolder.Status.SENT, provider.getProviderID(), messageID);
    }
}

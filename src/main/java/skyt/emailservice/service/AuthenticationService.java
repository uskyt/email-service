package skyt.emailservice.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;
import skyt.emailservice.dao.UserDAO;
import skyt.emailservice.vo.User;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.time.Instant;
import java.util.Base64;

/**
 * This class handles the crypto parts of password validation.
 *
 * The password hashing implemented here adheres to current best practice, as described in these links:
 *
 *  - https://crackstation.net/hashing-security.htm
 *  - https://www.baeldung.com/java-password-hashing
 *
 * A "salt" is generated and saved for each account, using a Cryptographically Secure Pseudo-Random Number Generator.
 * Using salt prevents "dictionary attacks" where an attacker has a large pre-built database of hashes for known
 * strings.
 *
 * Passwords are hashed together with their salt, using the "PBKDF2" algorithm, which is a deliberately "slow" hashing
 * algorithm. Using a slow algorithm makes it less feasible than otherwise to make brute force attacks.
 */
@Component
public class AuthenticationService {
    private static final Logger log = LogManager.getLogger(AuthenticationService.class);

    private static final int HASH_SIZE = 128;
    private static final int ITERATIONS = 65536;

    private SecureRandom secureRandom = new SecureRandom();
    private SecretKeyFactory factory;

    private UserDAO userDAO;

    @Autowired
    public AuthenticationService(UserDAO userDAO) throws NoSuchAlgorithmException {
        this.userDAO = userDAO;
        factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
    }

    /**
     * Main-method that can be used to create a User record manually.
     * Given a password a Salt and PasswordHash is printed.
     *
     * @param args password to be hashed.
     */
    public static void main(String[] args) throws NoSuchAlgorithmException {
        AuthenticationService service = new AuthenticationService(null);

        byte[] salt = service.generateSalt();
        String saltStr = Base64.getEncoder().encodeToString(salt);
        String password = args[0];
        String passwordHash = service.hash(salt, password);

        System.out.println("Salt: " + saltStr);
        System.out.println("PasswordHash: " + passwordHash);
    }

    private byte[] generateSalt() {
        byte[] salt = new byte[16];
        secureRandom.nextBytes(salt);
        return salt;
    }

    /**
     * Validate username-password against data in the Users table.
     */
    public boolean isPasswordCorrect(String username, String password) {
        try {
            User user = userDAO.getUserByUsername(username);
            boolean correct = isPasswordCorrect(user, password);
            if (correct) {
                userDAO.incrementRequestsCount(user.getId(), Instant.now());
            } else {
                userDAO.incrementAuthFailedCount(user.getId());
            }
            return correct;
        } catch (EmptyResultDataAccessException e) {
            log.warn("Unknown username '{}'", username);
            return false;
        }
    }

    private boolean isPasswordCorrect(User user, String password) {
        byte[] salt = Base64.getDecoder().decode(user.getSalt());
        String hash = hash(salt, password);
        boolean result = user.getPasswordHash().equals(hash);

        if (!result) {
            log.warn("Wrong password for username '{}'", user.getUsername());
        }
        return result;
    }

    private String hash(byte[] salt, String password) {
        try {
            KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, ITERATIONS, HASH_SIZE);
            byte[] hash = factory.generateSecret(spec).getEncoded();
            return Base64.getEncoder().encodeToString(hash);
        } catch (InvalidKeySpecException e) {
            log.error("Could not generate password hash", e);
            throw new RuntimeException("Internal error in authentication");
        }
    }
}

package skyt.emailservice.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import skyt.emailservice.dao.MessageDAO;
import skyt.emailservice.vo.Message;
import skyt.emailservice.vo.MessageHolder;


/**
 * This class takes messages to be sent. They are put in the "queue" (the Messages table) and then sent off to be
 * handled ASAP by the JobScheduler. A message is only processed from the "queue" if the primary flow has not finished
 * within a configurable period of time (e.g. one minute).
 */
@Component
public class QueueEmailService {
    private static final Logger log = LogManager.getLogger(QueueEmailService.class);

    @Autowired
    private JobScheduler jobScheduler;

    @Autowired
    private MessageDAO messageDAO;

    @Autowired
    private MessageValidator validator;

    /**
     * @param message The message to send.
     * @param requestedProviderID Optional argument, signals to use only the specified email provider.
     * @return A MessageHolder object that includes the message, its IDs, etc.
     */
    public MessageHolder queueEmail(Message message, String requestedProviderID) {
        // This will throw a kind of RuntimeException if validation fails
        validator.validateMessage(message);
        validator.validateRequestedProviderID(requestedProviderID);

        MessageHolder holder = new MessageHolder(message, requestedProviderID);
        messageDAO.createMessage(holder);
        jobScheduler.submitSendMessageJob(holder);
        return holder;
    }
}

package skyt.emailservice.service;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.PAYLOAD_TOO_LARGE)
public class MessageTooLargeException extends RuntimeException {
    public MessageTooLargeException(String message) {
        super(message);
    }
}

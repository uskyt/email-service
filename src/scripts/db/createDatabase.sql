CREATE DATABASE IF NOT EXISTS emailservice;
use emailservice;

CREATE TABLE `Messages` (
    `UUID` varchar(36) NOT NULL,
    `Created` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `Status` enum('REJECTED','QUEUED','SENT','ERROR_NOT_HANDLED','ERROR_HANDLED') NOT NULL DEFAULT 'QUEUED',
    `JSONMessage` longtext NOT NULL,
    `Modified` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `RequestedProviderID` varchar(30) DEFAULT NULL,
    `ProviderID` varchar(30) DEFAULT NULL,
    `MessageID` varchar(1000) DEFAULT NULL,
    `HandlingProcessUUID` varchar(36) DEFAULT NULL,
    PRIMARY KEY (`UUID`),
    KEY `IX_ModifiedByStatus` (`Status`,`Modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `Users` (
     `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
     `Username` varchar(20) NOT NULL DEFAULT '',
     `PasswordHash` varchar(256) NOT NULL DEFAULT '',
     `Salt` varchar(50) NOT NULL DEFAULT '',
     `Email` varchar(100) DEFAULT NULL,
     `FirstName` varchar(50) DEFAULT NULL,
     `LastName` varchar(50) DEFAULT NULL,
     `LastUsedTimestamp` timestamp NULL DEFAULT NULL,
     `AuthenticatedRequestsCount` int(11) NOT NULL DEFAULT '0',
     `AuthenticationFailedCount` int(11) NOT NULL DEFAULT '0',
     PRIMARY KEY (`id`),
     KEY `IX_Username` (`Username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

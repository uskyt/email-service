DROP USER IF EXISTS 'emailservice'@'localhost';
DROP USER IF EXISTS 'emailservice'@'%';
CREATE USER 'emailservice'@'localhost' IDENTIFIED BY 'Test1234';
CREATE USER 'emailservice'@'%' IDENTIFIED BY 'Test1234';
GRANT SELECT, DELETE, UPDATE, INSERT ON emailservice.* TO 'emailservice'@'localhost';
GRANT SELECT, DELETE, UPDATE, INSERT ON emailservice.* TO 'emailservice'@'%';
FLUSH PRIVILEGES;

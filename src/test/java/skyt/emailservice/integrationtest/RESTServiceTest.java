package skyt.emailservice.integrationtest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import skyt.emailservice.AbstractSpringBootTest;
import skyt.emailservice.ApplicationProperties;
import skyt.emailservice.TestHelper;
import skyt.emailservice.dao.MessageDAO;
import skyt.emailservice.rest.EmailController;
import skyt.emailservice.vo.Email;
import skyt.emailservice.vo.Message;
import skyt.emailservice.vo.MessageHolder;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RESTServiceTest extends AbstractSpringBootTest {
    private HttpClient client = HttpClientBuilder.create().build();
    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private ApplicationProperties properties;

    @Autowired
    private MessageDAO messageDAO;

    @Value("${api.username}")
    protected String api_username;

    @Value("${api.password}")
    protected String api_password;


    // @ApiResponse(code = 200, message = "Message queued successfully"),
    @Test
    public void send_basic_email_succeeds() throws IOException, ParseException {
        String subject = "Test ID=" + UUID.randomUUID().toString();
        Message message = TestHelper.createMessage();
        message.setSubject(subject);

        JSONObject jsonResponseObject = sendAndAssertSuccess(message);
        String id = (String) jsonResponseObject.get("id");

        MessageHolder messageHolder = messageDAO.readMessageByUuid(id);
        assertEquals(subject, messageHolder.getMessage().getSubject());
    }


    // @ApiResponse(code = 400, message = "JSON parse error: ${details}"),
    @Test
    public void send_fails_on_non_JSON_body() throws IOException, ParseException {
        String jsonStr = "This is not JSON";
        sendAndAssertError(jsonStr, 400, "JSON parse error: .*");
    }

    // @ApiResponse(code = 400, message = "Invalid email address '${email}' in the '${field}' field"),
    @Test
    public void send_fails_on_invalid_from_email() throws IOException, ParseException {
        Message message = TestHelper.createMessage();
        message.getFrom().setAddress("NOT_AN_EMAIL_ADDRESS");

        sendAndAssertError(message, 400, "Invalid email address 'NOT_AN_EMAIL_ADDRESS' in the 'from' field");
    }

    // @ApiResponse(code = 400, message = "Invalid email address '${email}' in the '${field}' field"),
    @Test
    public void send_fails_on_invalid_to_email() throws IOException, ParseException {
        Message message = TestHelper.createMessage();
        message.getTo()[0].setAddress("NOT_AN_EMAIL_ADDRESS");

        sendAndAssertError(message, 400, "Invalid email address 'NOT_AN_EMAIL_ADDRESS' in the 'to' field");
    }

    // @ApiResponse(code = 400, message = "Invalid email address '${email}' in the '${field}' field"),
    @Test
    public void send_fails_on_invalid_cc_email() throws IOException, ParseException {
        Message message = TestHelper.createMessage();
        Email[] emails = new Email[] {new Email("NOT_AN_EMAIL_ADDRESS")};
        message.setCc(emails);

        sendAndAssertError(message, 400, "Invalid email address 'NOT_AN_EMAIL_ADDRESS' in the 'cc' field");
    }

    // @ApiResponse(code = 400, message = "Invalid email address '${email}' in the '${field}' field"),
    @Test
    public void send_fails_on_invalid_bcc_email() throws IOException, ParseException {
        Message message = TestHelper.createMessage();
        Email[] emails = new Email[] {new Email("NOT_AN_EMAIL_ADDRESS")};
        message.setBcc(emails);

        sendAndAssertError(message, 400, "Invalid email address 'NOT_AN_EMAIL_ADDRESS' in the 'bcc' field");
    }

    // @ApiResponse(code = 400, message = "Too many recipients, current max is ${max}"),
    @Test public void send_fails_on_too_many_recipients() throws IOException, ParseException {
        Message message = TestHelper.createMessage();

        // Add too many recipients as BCC recipients
        List<Email> list = new ArrayList<>();
        for (int i=0; i<properties.validation_maxRecipients; i++) {
            list.add(new Email("ulrik.skyt@gmail.com"));
        }
        Email[] emails = list.toArray(new Email[list.size()]);
        message.setBcc(emails);

        sendAndAssertError(message, 400, "Too many recipients, current max is " + properties.validation_maxRecipients);
    }

    // @ApiResponse(code = 400, message = "Sender domain not whitelisted: ${domain}"),
    @Test public void send_fails_on_non_whitelisted_sender_domain() throws IOException, ParseException {
        Message message = TestHelper.createMessage();
        message.getFrom().setAddress("ulrik.skyt@gmail.com");

        sendAndAssertError(message, 400, "Sender domain not whitelisted: gmail.com");
    }

    // @ApiResponse(code = 400, message = "Unknown EmailProvider: ${emailProvider}"),
    @Test public void send_fails_on_unknown_email_provider() throws IOException, ParseException, URISyntaxException {
        Message message = TestHelper.createMessage();
        String jsonStr = objectMapper.writeValueAsString(message);

        HttpPost httpPost = prepareRequest(jsonStr, null);
        httpPost.setURI(new URI(getURL() + "?EmailProvider=unknown"));

        sendAndAssertError(httpPost, 400, "Unknown EmailProvider: 'unknown'");
    }

    // @ApiResponse(code = 401, message = "Invalid username or password"),
    @Test public void send_fails_on_unknown_user() throws IOException, ParseException {
        Message message = TestHelper.createMessage();
        String originalUsername = api_username;
        try {
            api_username = "unknown";
            sendAndAssertError(message, 401, "Invalid username or password");
        } finally {
            api_username = originalUsername;
        }
    }

    // @ApiResponse(code = 401, message = "Invalid username or password"),
    @Test public void send_fails_on_wrong_password() throws IOException, ParseException {
        Message message = TestHelper.createMessage();
        String originalPassword = api_password;
        try {
            api_password = "Not the right password";
            sendAndAssertError(message, 401, "Invalid username or password");
        } finally {
            api_password = originalPassword;
        }
    }

    // @ApiResponse(code = 401, message = "Invalid Authorization header"),
    @Test public void send_fails_on_invalid_authorization_header() throws IOException, ParseException {
        Message message = TestHelper.createMessage();
        String jsonStr = objectMapper.writeValueAsString(message);

        HttpPost httpPost = prepareRequest(jsonStr, null);
        httpPost.removeHeaders("Authorization");
        httpPost.addHeader("Authorization", "Not a valid Authorization header");

        sendAndAssertError(httpPost, 401, "Invalid Authorization header");
    }

    // @ApiResponse(code = 401, message = "Invalid Authorization header"),
    @Test public void send_fails_on_missing_authorization_header() throws IOException, ParseException {
        Message message = TestHelper.createMessage();
        String jsonStr = objectMapper.writeValueAsString(message);

        HttpPost httpPost = prepareRequest(jsonStr, null);
        httpPost.removeHeaders("Authorization");

        sendAndAssertError(httpPost, 401, "Invalid Authorization header");
    }

    // @ApiResponse(code = 413, message = "Message subject was too long"),
    @Test public void send_fails_on_too_large_subject() throws IOException, ParseException {
        Message message = TestHelper.createMessage();
        // By default subject length is not limited, so we need to test with a limit we set here
        int origMax = properties.validation_maxSubjectLength;
        try {
            properties.validation_maxSubjectLength = 20;
            message.setSubject("1234567890 1234567890");

            sendAndAssertError(message, 413, "Message subject was too long");
        } finally {
            properties.validation_maxSubjectLength = origMax;
        }
    }

    // @ApiResponse(code = 413, message = "Message body was too large"),
    @Test public void send_fails_on_too_large_body() throws IOException, ParseException {
        Message message = TestHelper.createMessage();
        // By default subject length is not limited, so we need to test with a limit we set here
        int origMax = properties.validation_maxBodyLength;
        try {
            properties.validation_maxBodyLength = 20;
            message.setBody("1234567890 1234567890");

            sendAndAssertError(message, 413, "Message body was too large");
        } finally {
            properties.validation_maxBodyLength = origMax;
        }
    }

    // @ApiResponse(code = 413, message = "Message total size was too large"),
    @Test public void send_fails_on_too_large_message_total_size() throws IOException, ParseException {
        Message message = TestHelper.createMessage();
        // By default total size is very big, so we just test with a smaller limit here...
        int origMax = properties.validation_maxTotalSize;
        try {
            properties.validation_maxTotalSize = TestHelper.LOREM_IPSUM.length() + 10; // Not enough for from+to+subject
            message.setBody(TestHelper.LOREM_IPSUM);

            sendAndAssertError(message, 413, "Message total size was too large");
        } finally {
            properties.validation_maxTotalSize = origMax;
        }
    }

    // @ApiResponse(code = 500, message = "Internal error"),
    @Test public void send_fails_correctly_on_unexpected_exception() throws IOException, ParseException {
        Message message = TestHelper.createMessage();

        // Setting NamedParameterJdbcTemplate to null in MessageDAO provokes a NullPointerException
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = null;
        try {
            namedParameterJdbcTemplate = messageDAO.setJdbcTemplate(null);
            sendAndAssertError(message, 500, ".*");
        } finally {
            messageDAO.setJdbcTemplate(namedParameterJdbcTemplate);
        }
    }

    // Could be tested using mocks that don't complete fast enough and sending enough messages to keep all http threads
    // busy and fill the request queue...
//    // @ApiResponse(code = 503, message = "Service unavailable")
//    @Test public void send_fails_on_() throws IOException, ParseException {
//        Message message = TestHelper.createMessage();
//        //message.
//
//        sendAndAssertError(message, 503, "Service unavailable");
//    }



    // ********************************************************************************
    // *** Helper methods
    // ********************************************************************************

    private JSONObject sendAndAssertSuccess(Message message) throws ParseException, IOException {
        String jsonStr = objectMapper.writeValueAsString(message);
        return sendAndAssertSuccess(jsonStr);
    }

    private JSONObject sendAndAssertSuccess(String jsonStr) throws ParseException, IOException {
        HttpPost httpPost = prepareRequest(jsonStr, null);
        HttpResponse response = client.execute(httpPost);
        String responseStr = IOUtils.toString(response.getEntity().getContent(), "UTF-8");

        assertEquals("Response: " + responseStr, 200, response.getStatusLine().getStatusCode());

        JSONObject jsonResponseObject = (JSONObject) new JSONParser().parse(responseStr);
        return jsonResponseObject;
    }

    private void sendAndAssertError(Message message, int expectedStatus, String errorMessageRegexp) throws IOException, ParseException {
        String jsonStr = objectMapper.writeValueAsString(message);
        sendAndAssertError(jsonStr, expectedStatus, errorMessageRegexp);
    }

    private void sendAndAssertError(String jsonStr, int expectedStatus, String errorMessageRegexp) throws IOException, ParseException {
        HttpPost httpPost = prepareRequest(jsonStr, null);
        sendAndAssertError(httpPost, expectedStatus, errorMessageRegexp);
    }

    private void sendAndAssertError(HttpPost httpPost, int expectedStatus, String errorMessageRegexp) throws IOException, ParseException {
        HttpResponse response = client.execute(httpPost);
        String responseStr = IOUtils.toString(response.getEntity().getContent(), "UTF-8");

        assertEquals("Response: " + responseStr, expectedStatus, response.getStatusLine().getStatusCode());

        JSONObject jsonResponseObject = (JSONObject) new JSONParser().parse(responseStr);

        String message = (String) jsonResponseObject.get("message");
        Pattern regexp = Pattern.compile(errorMessageRegexp, Pattern.DOTALL);
        Matcher m = regexp.matcher(message);
        assertTrue("Error message '" + message + "' does not match expected pattern: '" + errorMessageRegexp + "'", m.matches());

        Long status = (Long) jsonResponseObject.get("status");
        assertEquals(expectedStatus, status.intValue());
    }

    private HttpPost prepareRequest(String jsonStr, String provider) {
        String url = getURL();
        if (provider != null) {
            url += "?" + EmailController.PARAM_EMAIL_PROVIDER + "=" + provider;
        }
        HttpPost httpPost = new HttpPost(url);
        httpPost.addHeader("Content-type", "application/json");
        httpPost.addHeader("Authorization", "Basic " +
                Base64.getEncoder().encodeToString((api_username + ":" + api_password).getBytes()));
        httpPost.setEntity(new StringEntity(jsonStr, "UTF-8"));

        return httpPost;
    }
}

package skyt.emailservice.providers;

import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

public class MailgunEmailProviderTest extends AbstractEmailProviderTest {
    @Autowired
    private MailgunEmailProvider _provider;

    @PostConstruct
    private void postConstruct() {
        setEmailProvider(_provider);
    }
}

package skyt.emailservice.providers;

import com.github.tomakehurst.wiremock.http.HttpHeader;
import com.github.tomakehurst.wiremock.http.HttpHeaders;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import skyt.emailservice.AbstractSpringBootTest;
import skyt.emailservice.vo.Message;

import javax.annotation.PostConstruct;
import java.lang.reflect.Field;
import java.net.SocketTimeoutException;
import java.util.Date;
import java.util.UUID;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static skyt.emailservice.TestHelper.createMessage;

@DirtiesContext // The SendGrid @Component is configured to use a mock service
public class SendGridEmailProviderMockTest extends AbstractSpringBootTest {
    private static final Logger log = LogManager.getLogger(SendGridEmailProviderMockTest.class);
    private static final String SENDGRID_URL_PATH = "/v3/mail/send";
    private static final int PORT = 8010;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(PORT);

    @Autowired
    private SendGridEmailProvider provider;

    @PostConstruct
    private void init() {
        provider.sendGrid.setHost("localhost:" + PORT);

        // Hack to set a test state (i.e. use http instead of https) in the SendGrid 'Client' instance.
        try {
            Field testField = provider.client.getClass().getDeclaredField("test");
            testField.setAccessible(true);
            testField.set(provider.client, true);
        } catch (IllegalAccessException | NoSuchFieldException e) {
            log.error("Could not set 'test' state of SendGrid 'Client' instance", e);
        }
    }

    @Test
    public void test_normal_mock_service() {
        stubFor(post(urlEqualTo(SENDGRID_URL_PATH))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(202)
                        //.withBody(...) // A normal response does not have body content
                        .withHeaders(createSuccessfulHttpResponseHeaders())));

        Message message = createMessage();
        provider.sendEmail(message);
    }

    @Test
    public void test_socket_timeout_on_unresponsive_mock_service() {
        try {
            stubFor(post(urlEqualTo(SENDGRID_URL_PATH))
                    .withHeader("Accept", equalTo("application/json"))
                    .willReturn(aResponse()
                            .withFixedDelay(30000)
                            .withStatus(500)));

            Message message = createMessage();
            provider.sendEmail(message);
            fail("Expected a SocketTimeoutException");
        } catch (RuntimeException e) {
            assertTrue(e.getCause() instanceof SocketTimeoutException);
        }
    }

    private HttpHeaders createSuccessfulHttpResponseHeaders() {
        // These headers correspond to the headers returned by the real SendGrid service
        // Here, a few headers are generated. This is an example of the headers from the real service:
        //   X-Message-Id=ntyEucGfRF-HUzC5DRplsg
        //   Date=Sun, 03 Feb 2019 07:48:55 GMT
        return new HttpHeaders()
                .plus(new HttpHeader("Server", "nginx"))
                .plus(new HttpHeader("Access-Control-Allow-Origin", "https://sendgrid.api-docs.io"))
                .plus(new HttpHeader("Access-Control-Allow-Methods", "POST"))
                .plus(new HttpHeader("Connection", "keep-alive"))
                .plus(new HttpHeader("X-Message-Id", UUID.randomUUID().toString()))
                .plus(new HttpHeader("X-No-CORS-Reason", "https://sendgrid.com/docs/Classroom/Basics/API/cors.html"))
                .plus(new HttpHeader("Content-Length", "0"))
                .plus(new HttpHeader("Access-Control-Max-Age", "600"))
                .plus(new HttpHeader("Date", new Date().toString()))
                .plus(new HttpHeader("Access-Control-Allow-Headers", "Authorization, Content-Type, On-behalf-of, x-sg-elas-acl"))
                .plus(new HttpHeader("Content-Type", "text/plain; charset=utf-8"));
    }
}

package skyt.emailservice.providers;

import org.junit.Test;
import skyt.emailservice.AbstractSpringBootTest;
import skyt.emailservice.vo.Email;
import skyt.emailservice.vo.Message;

import static org.junit.Assert.assertNotNull;
import static skyt.emailservice.TestHelper.createMessage;

public abstract class AbstractEmailProviderTest extends AbstractSpringBootTest {

    protected EmailProvider provider;

    protected void setEmailProvider(EmailProvider provider) {
        this.provider = provider;
    }

    @Test
    public void send_mail() {
        Message message = createMessage();
        String messageID = provider.sendEmail(message);
        assertNotNull(messageID);
    }

    /**
     * Tests that errors from the Mailgun service results in an IOException.
     */
    @Test(expected = RuntimeException.class)
    public void send_mail_from_unsupported_domain_fails() {
        Message message = createMessage();
        message.setFrom(new Email("Not An Email Address", "not_an_email_address"));
        provider.sendEmail(message);
    }

}

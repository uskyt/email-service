package skyt.emailservice.providers;

import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

public class SparkpostEmailProviderTest extends AbstractEmailProviderTest {
    @Autowired
    private SparkpostEmailProvider _provider;

    @PostConstruct
    private void postConstruct() {
        setEmailProvider(_provider);
    }
}

package skyt.emailservice.providers;

import org.junit.Ignore;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

public class AmazonSESEmailProviderTest extends AbstractEmailProviderTest {
    @Autowired
    private AmazonSESEmailProvider _provider;

    @PostConstruct
    private void postConstruct() {
        setEmailProvider(_provider);
    }
}

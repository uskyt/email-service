package skyt.emailservice;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("/secret.properties")
public abstract class AbstractSpringBootTest {

    @LocalServerPort
    protected int port;

    protected String getURL() {
        return "http://localhost:" + port + "/rest/mail";
    }
}

package skyt.emailservice;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.assertj.core.util.Arrays;
import skyt.emailservice.vo.Email;
import skyt.emailservice.vo.Message;

import java.util.UUID;

public class TestHelper {
    private static final Logger log = LogManager.getLogger(TestHelper.class);

    public static final String LOREM_IPSUM = "Lorem ipsum dolor sit amet, eum euismod philosophia no. Disputando " +
            "instructior mei at, qui vero nobis referrentur in. Ius illum idque id. Soluta praesent referrentur id " +
            "pri, has possim noluisse salutatus ne, ei qui dicat primis vocent. Nec similique eloquentiam an. Ne vim " +
            "dolore eloquentiam, ne cibo exerci meliore his. ";

    public static Message createMessage() {
        String id = UUID.randomUUID().toString();
        log.info("Creating mail with id=" + id);

        Message message = new Message();

        message.setFrom(new Email("Skyt Consulting", "noreply@skytconsulting.dk"));
        message.setTo(Arrays.array(new Email("Ulrik Skyt", "usktrifork@gmail.com")));
        message.setSubject("Unit test message");
        message.setBody("Hi Ulrik,\n\nThis is a unit test message. I hope you don't receive too many.\n\nUnique id: "
                + id + "\n\nBest regards,\nSkyt Consulting");

        return message;
    }

}

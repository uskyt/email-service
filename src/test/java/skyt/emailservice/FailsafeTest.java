package skyt.emailservice;

import net.jodah.failsafe.CircuitBreaker;
import net.jodah.failsafe.Failsafe;
import net.jodah.failsafe.function.CheckedRunnable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.time.Duration;

import static net.jodah.failsafe.CircuitBreaker.State.*;
import static org.junit.Assert.*;

/**
 * This test class was written to verify some edge cases of how the net.jodah.Failsafe classes work.
 */
public class FailsafeTest {
    private static final Logger log = LogManager.getLogger(FailsafeTest.class);
    private CircuitBreaker<Void> circuitBreaker;

    private CheckedRunnable NOOP = () -> { };
    private CheckedRunnable FAIL = () -> {
        throw new RuntimeException("Test exception that leads to failure in CircuitBreaker");
    };


    @Before
    public void init() {
        circuitBreaker = new CircuitBreaker<Void>()
                .withFailureThreshold(3, 10)
                .withSuccessThreshold(5)
                .withDelay(Duration.ofMinutes(1));
    }

    @Test
    public void normal_call() {
        Failsafe.with(circuitBreaker).run(NOOP);

        assertEquals(0, circuitBreaker.getFailureCount());
        assertEquals(1, circuitBreaker.getSuccessCount());
        assertTrue(circuitBreaker.isClosed());
    }

    @Test
    public void exception_leading_to_circuit_breaker_failure() {
        try {
            Failsafe.with(circuitBreaker).run(() -> {
                throw new RuntimeException("Test exception that leads to failure in CircuitBreaker");
            });
        } catch (RuntimeException e) {
            assertEquals(1, circuitBreaker.getFailureCount());
            assertTrue(circuitBreaker.isClosed());
            return;
        }
        fail("Expected RuntimeException");
    }

    @Test
    @Ignore("Very slow test")
    public void test_close_open_delay_close_cycle() {
        callAndAssertState(NOOP, CLOSED);
        callAndAssertState(NOOP, CLOSED);
        callAndAssertState(NOOP, CLOSED);
        callAndAssertState(NOOP, CLOSED);
        callAndAssertState(NOOP, CLOSED);
        callAndAssertState(NOOP, CLOSED);
        callAndAssertState(NOOP, CLOSED);
        callAndAssertState(NOOP, CLOSED);
        callAndAssertState(NOOP, CLOSED);
        callAndAssertState(NOOP, CLOSED);
        callAndAssertState(NOOP, CLOSED);
        callAndAssertState(FAIL, CLOSED);
        callAndAssertState(FAIL, CLOSED);
        callAndAssertState(NOOP, CLOSED);
        callAndAssertState(FAIL, OPEN);
        callAndAssertState(FAIL, OPEN);
        callAndAssertState(NOOP, OPEN);
        callAndAssertState(NOOP, OPEN);
        callAndAssertState(NOOP, OPEN);
        callAndAssertState(NOOP, OPEN);
        callAndAssertState(NOOP, OPEN);
        callAndAssertState(NOOP, OPEN);
        callAndAssertState(NOOP, OPEN);
        callAndAssertState(NOOP, OPEN);
        callAndAssertState(NOOP, OPEN);
        callAndAssertState(NOOP, OPEN);
        callAndAssertState(NOOP, OPEN);
        callAndAssertState(NOOP, OPEN);
        callAndAssertState(NOOP, OPEN);
        callAndAssertState(NOOP, OPEN);
        callAndAssertState(NOOP, OPEN);
        callAndAssertState(NOOP, OPEN);
        callAndAssertState(NOOP, OPEN);
        callAndAssertState(NOOP, OPEN);
        callAndAssertState(NOOP, OPEN);
        callAndAssertState(NOOP, OPEN);
        callAndAssertState(NOOP, OPEN);
        callAndAssertState(NOOP, OPEN);
        log.info("Waiting for 1 minute delay to pass...");
        try {
            Thread.sleep(60000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("Finally the 1 minute delay has passed!");
        callAndAssertState(NOOP, HALF_OPEN);
        callAndAssertState(FAIL, OPEN);
        callAndAssertState(FAIL, OPEN);
        callAndAssertState(FAIL, OPEN);
        log.info("Waiting for 1 minute delay to pass...");
        try {
            Thread.sleep(60000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("Finally the 1 minute delay has passed!");
        callAndAssertState(NOOP, HALF_OPEN);
        callAndAssertState(NOOP, HALF_OPEN);
        callAndAssertState(NOOP, HALF_OPEN);
        callAndAssertState(NOOP, HALF_OPEN);
        callAndAssertState(NOOP, CLOSED);
        callAndAssertState(NOOP, CLOSED);
        callAndAssertState(NOOP, CLOSED);
    }

    private void callAndAssertState(CheckedRunnable r, CircuitBreaker.State expectedState) {
        try {
            Failsafe.with(circuitBreaker).run(r);
        } catch (RuntimeException e) {
          // Do nothing
        }
        assertEquals(expectedState, circuitBreaker.getState());
    }

}

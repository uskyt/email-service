package skyt.emailservice.dao;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import skyt.emailservice.AbstractSpringBootTest;
import skyt.emailservice.TestHelper;
import skyt.emailservice.vo.Message;
import skyt.emailservice.vo.MessageHolder;

import static org.junit.Assert.assertEquals;


public class MessageDAOTest extends AbstractSpringBootTest {

    @Autowired
    private MessageDAO messageDAO;

    @Test
    public void create_and_read() {
        Message msg = TestHelper.createMessage();
        MessageHolder msgHolder = new MessageHolder(msg);
        messageDAO.createMessage(msgHolder);

        MessageHolder msgHolder2 = messageDAO.readMessageByUuid(msgHolder.getUuid());

        assertMessageHoldersAreEqual(msgHolder, msgHolder2);
    }

    private void assertMessageHoldersAreEqual(MessageHolder mh1, MessageHolder mh2) {
        assertEquals(mh1.getUuid(), mh2.getUuid());
        assertEquals(mh1.getCreated(), mh2.getCreated());
        assertEquals(mh1.getStatus(), mh2.getStatus());
        assertEquals(mh1.getMessage(), mh2.getMessage());

        assertEquals(mh1.getModified(), mh2.getModified());
        assertEquals(mh1.getMessageID(), mh2.getMessageID());
        assertEquals(mh1.getProviderID(), mh2.getProviderID());
    }
}

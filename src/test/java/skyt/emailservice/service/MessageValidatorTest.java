package skyt.emailservice.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import skyt.emailservice.AbstractSpringBootTest;
import skyt.emailservice.ApplicationProperties;
import skyt.emailservice.TestHelper;
import skyt.emailservice.vo.Message;

import static org.junit.Assert.fail;

public class MessageValidatorTest extends AbstractSpringBootTest {

    // Some test examples were taken from https://help.xmatters.com/ondemand/trial/valid_email_format.htm
    // but it turned out that was *not* a good source as to what is correct vs.
    private static final String[] ADRESSES_WITH_BAD_PREFIX = {"abc..def@mail.com", ".abc@mail.com"};
    private static final String[] ADRESSES_WITH_VALID_PREFIX = {"abc-@mail.com", "abc-d@mail.com", "abc.def@mail.com", "abc@mail.com", "abc_def@mail.com", "abc#def@mail.com"};
    private static final String[] ADRESSES_WITH_BAD_DOMAIN = { "abc.def@#archive.com", "abc.def@mail", "abc.def@mail..com", "abc.def@mail.c"};
    private static final String[] ADRESSES_WITH_VALID_DOMAIN = {"abc.def@mail.cc", "abc.def@mail-archive.com", "abc.def@mail.org", "abc.def@mail.com"};

    private static final String[] ADRESSES_WITH_VALID_UTF8_CHARS = {"æblegrød@mail.cc", "abc.def@grønkål.com", "chang@电子邮件.cn", "电子邮件@mail.com"};


    @Autowired
    private MessageValidator messageValidator;

    @Autowired
    private ApplicationProperties properties;



    @Test(expected = MessageTooLargeException.class)
    public void too_large_body_should_be_rejected() {
        int maxSubjectLength = properties.validation_maxSubjectLength;
        int maxBodySize = properties.validation_maxBodyLength;
        int maxTotalSize = properties.validation_maxTotalSize;
        try {
            // Run this test with a limited maxBodyLength, but reset to original value afterwards
            properties.validation_maxSubjectLength = 0; // Unlimited
            properties.validation_maxBodyLength = 100000;
            properties.validation_maxTotalSize = 0; // Unlimited

            StringBuilder sb = new StringBuilder();
            while (sb.length() < properties.validation_maxBodyLength) {
                sb.append(TestHelper.LOREM_IPSUM);
            }

            Message msg = TestHelper.createMessage();
            msg.setBody(sb.toString());

            messageValidator.validateMessage(msg);
        } finally {
            properties.validation_maxSubjectLength = maxSubjectLength;
            properties.validation_maxBodyLength = maxBodySize;
            properties.validation_maxTotalSize = maxTotalSize;
        }
    }

    @Test(expected = MessageTooLargeException.class)
    public void too_large_subject_should_be_rejected() {
        int maxSubjectLength = properties.validation_maxSubjectLength;
        int maxBodySize = properties.validation_maxBodyLength;
        int maxTotalSize = properties.validation_maxTotalSize;
        try {
            // Run this test with a limited maxSubjectLength, but reset to original value afterwards
            properties.validation_maxSubjectLength = 100;
            properties.validation_maxBodyLength = 0; // Unlimited
            properties.validation_maxTotalSize = 0; // Unlimited

            Message msg = TestHelper.createMessage();
            msg.setSubject(TestHelper.LOREM_IPSUM);

            messageValidator.validateMessage(msg);
        } finally {
            properties.validation_maxSubjectLength = maxSubjectLength;
            properties.validation_maxBodyLength = maxBodySize;
            properties.validation_maxTotalSize = maxTotalSize;
        }
    }

    @Test(expected = MessageTooLargeException.class)
    public void too_large_total_message_should_be_rejected() {
        int maxSubjectLength = properties.validation_maxSubjectLength;
        int maxBodySize = properties.validation_maxBodyLength;
        int maxTotalSize = properties.validation_maxTotalSize;
        try {
            // Run this test with a limited maxTotalSize, but reset to original value afterwards
            properties.validation_maxSubjectLength = 0; // Unlimited
            properties.validation_maxBodyLength = 0; // Unlimited
            properties.validation_maxTotalSize = 100000;

            StringBuilder sb = new StringBuilder();
            while (sb.length() < properties.validation_maxTotalSize) {
                sb.append(TestHelper.LOREM_IPSUM);
            }

            Message msg = TestHelper.createMessage();
            msg.setBody(sb.toString());

            messageValidator.validateMessage(msg);
        } finally {
            properties.validation_maxSubjectLength = maxSubjectLength;
            properties.validation_maxBodyLength = maxBodySize;
            properties.validation_maxTotalSize = maxTotalSize;
        }
    }

    @Test
    public void emails_with_bad_prefixes_should_fail(){
        testAddressesThatShouldFail(ADRESSES_WITH_BAD_PREFIX, "a bad prefix");
    }

    @Test
    public void emails_with_valid_prefixes_should_succeed(){
        testAddressesThatShouldSucceed(ADRESSES_WITH_VALID_PREFIX);
    }

    @Test
    public void emails_with_bad_domains_should_fail(){
        testAddressesThatShouldFail(ADRESSES_WITH_BAD_DOMAIN, "a bad domain");
    }

    @Test
    public void emails_with_valid_domains_should_succeed(){
        testAddressesThatShouldSucceed(ADRESSES_WITH_VALID_DOMAIN);
    }

    @Test
    public void emails_with_valid_utf8_characters_should_succeed(){
        testAddressesThatShouldSucceed(ADRESSES_WITH_VALID_UTF8_CHARS);
    }


    private void testAddressesThatShouldFail(String[] adresses, String reason) {
        for (String emailAddress : adresses) {
            try {
                messageValidator.validateEmailAddress(emailAddress, "to");
                fail("Expected ValidationException on email address '" + emailAddress + "' because of " + reason);
            } catch (ValidationException e) {
                // OK
            }
        }
    }

    private void testAddressesThatShouldSucceed(String[] adresses) {
        for (String emailAddress : adresses) {
            messageValidator.validateEmailAddress(emailAddress, "to");
        }
    }

}

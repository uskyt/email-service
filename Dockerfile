# https://www.baeldung.com/dockerizing-spring-boot-application

FROM alpine:edge
MAINTAINER ulrik.skyt@gmail.com
RUN apk add --no-cache openjdk8
COPY files/UnlimitedJCEPolicyJDK8/* \
  /usr/lib/jvm/java-1.8-openjdk/jre/lib/security/
